export function createRocketOntology(config) {
  return {
    engines: config.engines || [
      { ips: 0, status: 'off', setImpulse: function(value) { /*...*/ } },
      { ips: 0, status: 'off', setImpulse: function(value) { /*...*/ } },
      { ips: 0, status: 'off', setImpulse: function(value) { /*...*/ } },
      { ips: 0, status: 'off', setImpulse: function(value) { /*...*/ } }
    ],
    fuelCell: config.fuelCell || {
      capacity: 1000, 
      currentFuel: 1000,
    },
    position: config.position || { x: 0, y: 0, z: 0 },
    velocity: config.velocity || { x: 0, y: 0, z: 0 },
  };
}

export function createRocketExperienceHandlers(rocketOntology) {
  return [
    {
      type: 'RocketApplyThrust',
      handler: (experience) => {
        const force = experience.get().content.force;
        rocketOntology.engines.forEach(engine => {
          if (engine.status === 'on') {
            engine.ips = force; // Update the impulse value
          }
        });
        return null;
      }
    },
    {
      type: 'RocketSetEngineStatus',
      handler: (experience) => {
        const idx = experience.get().content.idx;
        rocketOntology.engines[idx].status = experience.content.status;
        return null;
      }
    },
    {
      type: 'RocketConsumeFuel',
      handler: (experience) => {
        const duration = experience.get().content.duration;
        const totalImpulse = rocketOntology.engines.reduce((sum, engine) => sum + engine.ips, 0);
        rocketOntology.fuelCell.currentFuel -= totalImpulse * duration; // Update fuel level
        return null;
      }
    },
    {
      type: 'RocketCalculateThrustPeriod',
      handler: (experience) => {
        const duration = experience.get().content.duration;
        const totalImpulse = rocketOntology.engines.reduce((sum, engine) => sum + engine.ips, 0);
        const force = totalImpulse * duration;
        rocketOntology.fuelCell.currentFuel -= totalImpulse * duration; // Update fuel level
        rocketOntology.position.x += rocketOntology.velocity.x * duration;
        rocketOntology.position.y += rocketOntology.velocity.y * duration;
        rocketOntology.position.z += rocketOntology.velocity.z * duration;
        return force;
      }
    }
  ];
}

export function createThreeJSSceneOntology(config) {
  return {
    scene: config.scene,
    canvasElement: config.canvasElement,
    renderer: config.renderer,

    clock: config.clock,
    stats: config.stats,

    ambientLight: config.ambientLight,
    directionalLight: config.directionalLight,
  }
}

export function createThreeJSSceneExperienceHandlers(threeJSSceneOntology) {
  return [
    {
      type: 'ThreeInitialise',
      handler: (experience) => {
        threeJSSceneOntology.scene = new THREE.Scene();
    
        threeJSSceneOntology.renderer = new THREE.WebGLRenderer({
          canvas: experience.get().content.canvasElement,
          antialias: true,
        });
        threeJSSceneOntology.renderer.setSize(window.innerWidth, window.innerHeight);
        document.body.appendChild(threeJSSceneOntology.renderer.domElement);
    
        threeJSSceneOntology.clock = new THREE.Clock();
        threeJSSceneOntology.stats = Stats();
        document.body.appendChild(threeJSSceneOntology.stats.dom);
    
        threeJSSceneOntology.ambientLight = new THREE.AmbientLight(0xffffff, 0.5);
        threeJSSceneOntology.ambientLight.castShadow = true;
        threeJSSceneOntology.scene.add(threeJSSceneOntology.ambientLight);
    
        this.directionalLight = new THREE.DirectionalLight(0xffffff, 1);
        this.directionalLight.position.set(0, 32, 64);
        this.scene.add(this.directionalLight);
    
        window.addEventListener('resize', () => {
          this.renderer.setSize(window.innerWidth, window.innerHeight);
        }, false);
        return null;
      }
    },
    {
      type: 'ThreeAddObject',
      handler: (experience) => {
        threeJSSceneOntology.scene.add(experience.get().content.object);
        return null;
      }
    }
  ];
}