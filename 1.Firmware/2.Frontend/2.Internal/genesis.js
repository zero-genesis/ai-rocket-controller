export class Omnistructure {
  constructor(ontology) {
    this.ontology = ontology;
  }

  get() {
    return this.ontology;
  }

  set(newOntology) {
    this.ontology = newOntology;
  }

  transform(transformFunction) {
    if (typeof transformFunction === 'function') {
      this.ontology = transformFunction(this.ontology);
    } else {
      console.error('The provided argument is not a function');
    }
  }

  toString() {
    return JSON.stringify(this.ontology);
  }

  updateFields(updatedFields) {
    if (typeof updatedFields !== 'object') {
      console.error('The provided argument is not an object');
      return;
    }

    this.ontology = { ...this.ontology, ...updatedFields };
  }

  addField(key, value) {
    if (key in this.ontology) {
      console.warn('The field already exists. Use updateFields to update existing fields.');
      return;
    }

    this.ontology[key] = value;
  }

  removeField(key) {
    if (!(key in this.ontology)) {
      console.warn('The field does not exist.');
      return;
    }

    delete this.ontology[key];
  }
}

export class Construct extends Omnistructure {
  constructor(ontology, type) {
    super({});
    this.set({ ...ontology, type });
  }
}

export class Consciousness extends Omnistructure {
  constructor(ontology) {
    super({});
    this.set({
      ...ontology,
      decisions: [],
      existence: null,
      universe: null,
      experienceHandlers: [],
    });
  }

  addDecision(decision) {
    this.updateFields({ decisions: [...this.get().decisions, decision] });
  }

  setExistence(existence) {
    if (existence instanceof Existence) {
      this.updateFields({ existence: existence });
    } else {
      console.error('The provided argument is not an instance of Existence');
    }
  }

  setUniverse(universe) {
    if (universe instanceof Universe) {
      this.updateFields({ universe: universe });
    } else {
      console.error('The provided argument is not an instance of Universe');
    }
  }

  executeDecision(decision) {
    if (decision instanceof Decision) {
      decision.execute();
    } else {
      console.error('The provided argument is not an instance of Decision');
    }
  }

  executeOwnDecisions() {
    const decisions = this.get().decisions;
    decisions.forEach(decision => {
      if (decision instanceof Decision) {
        decision.execute();
      } else {
        console.error('The item in the decisions array is not an instance of Decision');
      }
    });
    this.updateFields({ decisions: [] }); // Clear the decisions array after execution
  }

  handleExperience(experience) {
    const experienceHandlers = this.get().experienceHandlers;
    const relevantHandler = experienceHandlers.find(handler => handler.type === experience.get().type);
    if (relevantHandler && typeof relevantHandler.handler === 'function') {
      const decision = relevantHandler.handler(experience);
      if (decision instanceof Decision) {
        this.addDecision(decision);
      }
    }
  }

  addExperienceHandler(handler) {
    if (typeof handler.type !== 'string' || typeof handler.handler !== 'function') {
      console.error('Invalid handler format. Handlers must have a type (string) and a handler (function).');
      return;
    }
    this.updateFields({
      experienceHandlers: [...this.get().experienceHandlers, handler],
    });
  }
}

export class Experience extends Construct {
  constructor(ontology) {
    super({}, 'Experience');
    this.set({ ...ontology });
  }
}

export class Decision extends Construct {
  constructor(ontology) {
    super(ontology, 'Decision');
    if (!ontology.action || typeof ontology.action !== 'function') {
      throw new Error('A Decision must have an action property that is a function.');
    }
  }

  execute() {
    this.get().action();
  }
}

export class Shell extends Construct {
  constructor(ontology, consciousness) {
    super(ontology, 'Shell');
    this.updateFields({
      consciousness: consciousness,
      messageHandlers: [],
    });
  }

  interpretMessage(message) {
    if (message instanceof Message) {
      const experience = new Experience({ content: message.get().content, type: message.get().type });
      this.handleMessage(experience);
    } else {
      console.error('Received object is not an instance of Message');
    }
  }

  handleMessage(experience) {
    const messageHandlers = this.get().messageHandlers;
    const relevantHandler = messageHandlers.find(handler => handler.type === experience.get().type);
    if (relevantHandler && typeof relevantHandler.handler === 'function') {
      const modifiedExperience = relevantHandler.handler(experience);
      if (modifiedExperience instanceof Experience) {
        this.get().consciousness.handleExperience(modifiedExperience);
      }
    } else {
      // If no handler is found, pass the experience as is to the consciousness
      this.get().consciousness.handleExperience(experience);
    }
  }

  addMessageHandler(handler) {
    if (typeof handler.type !== 'string' || typeof handler.handler !== 'function') {
      console.error('Invalid handler format. Handlers must have a type (string) and a handler (function).');
      return;
    }
    this.updateFields({
      messageHandlers: [...this.get().messageHandlers, handler],
    });
  }
}

export class Existence extends Construct {
  constructor(ontology, shell) {
    super({}, 'Existence');
    const consciousness = new Consciousness({});

    this.set({
      ...ontology,
      shell: shell,
    });

    consciousness.setExistence(this);
  }

  interactWithUniverse(universe) {
    this.get().shell.get().consciousness.setUniverse(universe);
  }

  receiveMessage(message) {
    if (message instanceof Message) {
      this.get().shell.interpretMessage(message);
    } else {
      console.error('Received object is not an instance of Message');
    }
  }
}

export class Universe extends Construct {
  constructor(ontology) {
    super({}, 'Universe');
    this.set({
      ...ontology,
      existences: [],
      decisionQueue: [],
    });

    // Start the universal update loop
    // setInterval(() => {
    //   this.executeDecisions();
    //   this.executeConsciousnessDecisions();
    // }, 1000);
  }

  addExistence(existence) {
    if (existence instanceof Existence) {
      this.updateFields({ existences: [...this.get().existences, existence] });
      existence.interactWithUniverse(this);
    } else {
      console.error('The provided argument is not an instance of Existence');
    }
  }

  addDecision(decision) {
    if (decision instanceof Decision) {
      this.get().decisionQueue.push(decision);
    } else {
      console.error('The provided argument is not an instance of Decision');
    }
  }

  executeDecisions() {
    const decisions = this.get().decisionQueue;
    decisions.forEach(decision => decision.execute());
    this.updateFields({ decisionQueue: [] }); // Clear the queue after execution
  }

  executeConsciousnessDecisions() {
    const existences = this.get().existences;
    existences.forEach(existence => {
      const consciousness = existence.get().consciousness;
      if (consciousness) {
        consciousness.executeOwnDecisions();
      }
    });
  }

  broadcastMessage(message) {
    if (message instanceof BroadcastMessage || message instanceof PartialMessage) {
      this.get().existences.forEach(existence => {
        if (message instanceof PartialMessage) {
          if (message.get().conditionFunction(existence)) {
            existence.receiveMessage(message);
          }
        } else {
          existence.receiveMessage(message);
        }
      });
    } else {
      console.error('Message must be an instance of BroadcastMessage or PartialMessage for broadcasting.');
    }
  }

  sendMessage(message) {
    if (message instanceof DirectMessage) {
      const targetExistence = this.get().existences.find(e => e.get().id === message.get().targetExistenceId);
      if (targetExistence) {
        targetExistence.receiveMessage(message);
      } else {
        console.error('Target existence not found.');
      }
    } else {
      console.error('Message must be an instance of DirectMessage for direct sending.');
    }
  }
}

export class Message extends Construct {
  constructor(content, type) {
    super({ content, type }, 'Message');
  }
}

export class BroadcastMessage extends Message {
  constructor(content) {
    super(content, 'Broadcast');
  }
}

export class DirectMessage extends Message {
  constructor(content, targetExistenceId) {
    super(content, 'Direct');
    this.updateFields({ targetExistenceId });
  }
}

export class PartialMessage extends Message {
  constructor(content, conditionFunction) {
    super(content, 'Partial');
    if (typeof conditionFunction !== 'function') {
      throw new Error('Condition function must be a function.');
    }
    this.updateFields({ conditionFunction: conditionFunction });
  }
}