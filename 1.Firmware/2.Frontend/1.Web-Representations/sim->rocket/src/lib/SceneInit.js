import * as THREE from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import Stats from 'three/examples/jsm/libs/stats.module';
import { GLTFLoader } from 'three/addons/loaders/GLTFLoader.js';
import { CSS2DRenderer } from 'three/addons/renderers/CSS2DRenderer.js';

export default class SceneInit {
  constructor(canvasElement) {
    this.scene = new THREE.Scene();
    this.cameras = []; // Store multiple cameras
    this.activeCameraIndex = 0; // Index of the active camera
    this.cameraTargets = [];
    this.previousMeshPositions = [];
    this.renderer = new THREE.WebGLRenderer({
      logarithmicDepthBuffer: true,
      canvas: canvasElement,
      antialias: true,
    });
    this.renderer.setSize(window.innerWidth, window.innerHeight);

    this.clock = new THREE.Clock();
    this.stats = Stats();
    document.body.appendChild(this.stats.dom);

    this.ambientLight = new THREE.AmbientLight(0xffffff, 0.5);
    this.scene.add(this.ambientLight);

    this.directionalLight = new THREE.DirectionalLight(0xffffff, 1);
    this.directionalLight.position.set(0, 32, 64);
    this.directionalLight.castShadow = true;

    this.renderer.gammaOutput = true;
    this.renderer.gammaFactor = 2.2;

    this.scene.add(this.directionalLight);

    this.gltfLoader = new GLTFLoader();

    this.labelRenderer = new CSS2DRenderer();
    this.labelRenderer.setSize(window.innerWidth, window.innerHeight);
    this.labelRenderer.domElement.style.position = 'absolute'; // The label renderer should be in the same position as the canvas
    this.labelRenderer.domElement.style.top = '0px';
    document.body.appendChild(this.labelRenderer.domElement); // Append it to the body element

    // Ensure the canvas has a z-index lower than the labelRenderer to prevent canvas overlay
    this.renderer.domElement.style.position = 'absolute';
    this.renderer.domElement.style.zIndex = '1';
    this.labelRenderer.domElement.style.zIndex = '2';
    this.labelRenderer.domElement.style.pointerEvents = 'none';

    window.addEventListener('resize', () => this.onWindowResize(), false);
  }

  addCamera(fov = 45, aspect = window.innerWidth / window.innerHeight, near = 1, far = 1000) {
    const camera = new THREE.PerspectiveCamera(fov, aspect, near, far);
    this.cameras.push(camera);
    return camera;
  }

  removeCamera(camera) {
    const index = this.cameras.indexOf(camera);
    if (index > -1) {
      this.cameras.splice(index, 1);
    }
  }

  setActiveCamera(index) {
    if (index >= 0 && index < this.cameras.length) {
      this.activeCameraIndex = index;
      this.controls = new OrbitControls(this.activeCamera, this.renderer.domElement);

      const target = this.cameraTargets[index];
      if (target) {
        this.controls.target.copy(target.position);
      }
    }
  }

  get activeCamera() {
    return this.cameras[this.activeCameraIndex];
  }

  animate() {
    if (this.cameras.length > 0) {
      const target = this.cameraTargets[this.activeCameraIndex];
      if (target) {
        const previousPosition = this.previousMeshPositions[this.activeCameraIndex];

        if (previousPosition) {
          // Calculate the delta position
          const deltaPosition = target.position.clone().sub(previousPosition);
          
          // Apply the delta position to the camera
          this.activeCamera.position.add(deltaPosition);
          
          // Update the controls target
          this.controls.target.add(deltaPosition);

          // Store the current position for the next frame
          this.previousMeshPositions[this.activeCameraIndex] = target.position.clone();
        } else {
          // If there is no previous position stored, store the current one
          this.previousMeshPositions[this.activeCameraIndex] = target.position.clone();
        }
      }
      this.render();
    }
  }

  setCameraTarget(cameraIndex, mesh) {
    if (cameraIndex >= 0 && cameraIndex < this.cameras.length) {
      this.cameraTargets[cameraIndex] = mesh;

      // Store the initial position of the mesh
      this.previousMeshPositions[cameraIndex] = mesh.position.clone();

      // If the target is set for the active camera, update the controls immediately
      if (cameraIndex === this.activeCameraIndex) {
        this.controls.target.copy(mesh.position);
      }
    }
  }

  attachGltfModelToMesh(gltfPath, mesh, position = null, scale = null) {
    this.gltfLoader.load(gltfPath, (gltf) => {
      const model = gltf.scene;
      model.position.copy(position || mesh.position);
      
      const modelScale = scale || new THREE.Vector3(1, 1, 1);
      model.scale.copy(modelScale);

      mesh.add(model);
    }, undefined, (error) => {
      console.error('An error happened with the GLTFLoader', error);
    });
  }

  render() {
    if (this.cameras.length > 0) {
      this.renderer.render(this.scene, this.activeCamera);
      this.labelRenderer.render(this.scene, this.activeCamera);
      this.stats.update();
      this.controls.update();
    }
  }

  onWindowResize() {
    this.cameras.forEach(camera => {
      camera.aspect = window.innerWidth / window.innerHeight;
      camera.updateProjectionMatrix();
    });
    this.renderer.setSize(window.innerWidth, window.innerHeight);
    this.labelRenderer.setSize(window.innerWidth, window.innerHeight);
  }
}
