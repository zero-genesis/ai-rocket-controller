import * as THREE from 'three';
import * as CANNON from 'cannon-es';

export const RocketLibrary = {
  createEngines(options, count) {
    const engines = [];
    for (let i = 0; i < count; i++) {
      engines.push({
        radius: options.radius,
        thrust: options.thrust,
        specificImpulse: 300,
        throttle: options.throttle,
        gimbal: options.gimbal,
        
      });
    }
    return engines;
  },

  positionEngines(radius, layerDistances, pointsPerLayer) {
    if (pointsPerLayer.length !== layerDistances.length) {
      throw new Error("pointsPerLayer and layerDistances must be of the same length");
    }
    const layers = [];
    for (let layer = 0; layer < layerDistances.length; layer++) {
      layers.push({ positions: [] });
      const numPoints = pointsPerLayer[layer];
      const layerRadius = layerDistances[layer] * radius;
      const isOddLayer = (layer + 1) % 2 !== 0;
      const angleOffset = isOddLayer ? ((2 * Math.PI) / numPoints)/2 : 0;
      for (let i = 0; i < numPoints; i++) {
        const angle = ((i / numPoints) * 2 * Math.PI) + angleOffset;
        const x = layerRadius * Math.cos(angle);
        const y = layerRadius * Math.sin(angle);
        layers[layer].positions.push({ x: x, y: y });
      }
    }
    return layers;
  },

  generateEngineMeshes(layers, stage, shellHeight, engineRadius) {
    const engineMeshes = {};
    const height = (-1) * (shellHeight / 2) + engineRadius/2;
    layers.forEach((layer, layerIndex) => {
      layer.positions.forEach((position, engineIndex) => {
        const meshName = `stage${stage}_layer${layerIndex}_engine${engineIndex}`;
        const engineMesh = new THREE.Mesh(
          new THREE.SphereGeometry(engineRadius, 8, 8),
          new THREE.MeshBasicMaterial({
            color: 0xFFA500,
            wireframe: true
          }),
        );
        engineMesh.position.set(position.x, height, position.y);
        engineMeshes[meshName] = engineMesh;
      });
    });
    return engineMeshes;
  },

  generateEngineBodies(layers, stage, shellHeight, engineMass, engineRadius) {
    const engineBodies = {};
    const height = (-1) * (shellHeight / 2) + engineRadius/2;
    layers.forEach((layer, layerIndex) => {
      layer.positions.forEach((position, engineIndex) => {
        const bodyName = `stage${stage}_layer${layerIndex}_engine${engineIndex}`;
        const engineBody = new CANNON.Body({
          mass: engineMass,
          shape: new CANNON.Sphere(engineRadius)
        });
        engineBody.position.set(position.x, height, position.y);
        engineBodies[bodyName] = engineBody;
      });
    });
    return engineBodies;
  },

  createRocketOptions(rocketOptions) {
    let meshes = {};
    let bodies = {};
  
    const stages = rocketOptions.stages.map((stage, index) => {
      const engineLayout = this.positionEngines(stage.shell.radius, stage.engineLayerDistances, stage.pointsPerLayer);
      const engines = this.createEngines(stage.engineOptions, stage.numberOfEngines);
  
      const stageMesh = new THREE.Mesh(
        new THREE.CylinderGeometry(stage.shell.radius, stage.shell.radius, stage.shell.height, 8),
        new THREE.MeshBasicMaterial({
          color: 0xFFA500,
          wireframe: true
        }),
      );
      meshes[`stage${index}_shell`] = stageMesh;
  
      const stageBody = new CANNON.Body({
        mass: stage.shell.mass + stage.fuelCells.fuelMass,
        shape: new CANNON.Cylinder(stage.shell.radius, stage.shell.radius, stage.shell.height, 8)
      });
      bodies[`stage${index}_shell`] = stageBody;
  
      meshes = {...meshes, ...this.generateEngineMeshes(engineLayout, index, stage.shell.height, stage.engineOptions.radius, meshes)};
      bodies = {...bodies, ...this.generateEngineBodies(engineLayout, index, stage.shell.height, stage.engineOptions.mass, stage.engineOptions.radius, bodies)};
  
      return {
        ...stage,
        engineLayout,
        engines,
      };
    });
  
    return {
      id: rocketOptions.id,
      type: 'rocket',
      state: {
        name: rocketOptions.name,
        stages,
      },
      meshes,
      bodies,
      reactions: {
        setThrottle: [{
          handler: this.setThrottle,
        }],
      },
      onCreate: this.onCreate,
      update: this.update,
    };
  },

  setGimbals(rocketEntity, cosm) {
    rocketEntity.state.stages.forEach((stage, stageIndex) => {
      const stageBody = rocketEntity.bodies[`stage${stageIndex}_shell`];
      stage.engineLayout.forEach((layer, layerIndex) => {
        layer.positions.forEach((position, engineIndex) => {
          const engineBodyKey = `stage${stageIndex}_layer${layerIndex}_engine${engineIndex}`;
          const engineBody = rocketEntity.bodies[engineBodyKey];
          engineBody.quaternion.set(stageBody.quaternion.x, stageBody.quaternion.y, stageBody.quaternion.z, stageBody.quaternion.w)
        });
      });
    });
  },

  applyThrust(rocketEntity, cosm, planetId) {
    // Cannone ES will multiply the force times delta time so you don't need to do it yourself
    // But you do need to take it into account for fuel consumption
    if(rocketEntity.state.stages[0].status === 'active' && rocketEntity.state.stages[0].engines[0].throttle === 1) {
      console.log("Time", performance.now()/1000);
    }
    const G = 6.67430e-11; // Gravitational constant
    const deltaTime = cosm.elapsedTime;
    const planetEntity = cosm.entities.find(entity => entity.id === planetId);
    if (!planetEntity) {
      console.warn(`Planet with ID ${planetId} not found`);
      return;
    }

    const planetMass = planetEntity.state.mass;
    const planetRadius = planetEntity.meshes.planet.geometry.parameters.radius;

    const forcesToApply = [];
    let totalThrustForce = new CANNON.Vec3(0, 0, 0);

    rocketEntity.state.stages.forEach((stage, stageIndex) => {
      if (stage.status === 'active') {
        const stageBody = rocketEntity.bodies[`stage${stageIndex}_shell`]
        const rocketPosition = rocketEntity.bodies[`stage${stageIndex}_shell`].position;
        const planetPosition = planetEntity.bodies.planet.position;
        const differenceVector = rocketPosition.vsub(planetPosition);
        const rocketAltitude = differenceVector.length() - planetRadius;

        const gravity = G * planetMass / Math.pow(rocketAltitude + planetRadius, 2);
        const gravitationalAcceleration = gravity;
  
        let engineIdx = 0;
        stage.engineLayout.forEach((layer, layerIndex) => {
          layer.positions.forEach((position, engineIndex) => {
            const engine = stage.engines[engineIdx];
            if (engine && engine.thrust > 0 && engine.throttle > 0) {
              const throttle = engine.throttle;
              const fuelConsumption =  ((throttle * engine.thrust) / (engine.specificImpulse * gravitationalAcceleration)) * deltaTime;
              const actualFuelConsumption = Math.min(fuelConsumption, stage.fuelCells.fuelMass);

              stage.fuelCells.fuelMass -= actualFuelConsumption;
              stageBody.mass -= actualFuelConsumption;
              stageBody.updateMassProperties(); 
  
              if (stage.fuelCells.fuelMass <= 0) {
                engine.thrust = 0;
                stage.fuelCells.fuelMass = 0;
                stage.status = 'depleted';
              } else {
                const thrustForce = engine.thrust * throttle;
                const thrustDirection = new CANNON.Vec3(0, 1, 0);
                const forceVector = thrustDirection.scale(thrustForce);
                const engineBodyKey = `stage${stageIndex}_layer${layerIndex}_engine${engineIndex}`;
                const engineBody = rocketEntity.bodies[engineBodyKey];
                const forcePosition = new CANNON.Vec3(position.x, -stage.shell.height/2, position.y);
  
                if (engineBody) {
                  forcesToApply.push({ force: forceVector, body: stageBody, position: forcePosition });
                } else {
                  console.warn(`Body for ${engineBodyKey} not found`);
                }
              }
              engineIdx += 1;
            }
          });
        });
      }
    });
    
    forcesToApply.forEach(({ force, body, position }) => {
      totalThrustForce = totalThrustForce.vadd(force);
      body.applyLocalForce(force, position);
    });

    console.log("Total Thrust Force: ", totalThrustForce.toString(), totalThrustForce.length());
    console.log("First Stage Fuel: ", rocketEntity.state.stages[0].fuelCells.fuelMass/rocketEntity.state.stages[0].fuelCells.capacity);
  },

  applyGravity(rocketEntity, cosm, planetId) {
    // Cannone ES will multiply the force times delta time so you don't need to do it yourself
    const G = 6.67430e-11; // Gravitational constant
    const planetEntity = cosm.entities.find(entity => entity.id === planetId);
    if (!planetEntity) {
      console.warn(`Planet with ID ${planetId} not found`);
      return;
    }
  
    const planetMass = planetEntity.state.mass;
    const planetRadius = planetEntity.meshes.planet.geometry.parameters.radius;

    let totalGravityForce = new CANNON.Vec3(0, 0, 0);
  
    rocketEntity.state.stages.forEach((stage, stageIndex) => {
      const rocketPosition = rocketEntity.bodies[`stage${stageIndex}_shell`].position;
      const planetPosition = planetEntity.bodies.planet.position;
      const differenceVector = rocketPosition.vsub(planetPosition);
      const rocketAltitude = differenceVector.length() - planetRadius;

      const stageBody = rocketEntity.bodies[`stage${stageIndex}_shell`];

      const gravityForceMagnitude = G * planetMass * stageBody.mass / Math.pow(rocketAltitude + planetRadius, 2);
      
      differenceVector.normalize();
      const gravityForceVector = differenceVector.scale(-gravityForceMagnitude);

      if (stageBody) {
        // Convert the global force vector to local coordinates
        const localGravityForceVector = stageBody.quaternion.inverse().vmult(gravityForceVector);
        totalGravityForce = totalGravityForce.vadd(localGravityForceVector);
        stageBody.applyLocalForce(localGravityForceVector, new CANNON.Vec3(0, 0, 0)); // Apply at the center of mass
      } else {
        console.warn(`Body for stage${stageIndex}_shell not found`);
      }
    });
    
    console.log("Total Gravity Force: ", totalGravityForce.toString(), totalGravityForce.length());
  },

  build(rocketEntity, cosm) {
    // Collision groups
    const COLLISION_GROUP_ROCKET = 1;
    const COLLISION_GROUP_ENGINE = 2;
  
    rocketEntity.state.stages.forEach((stage, stageIndex) => {
    
      // Adjust position of the rocket shell mesh and body
      const rocketMesh = rocketEntity.meshes[`stage${stageIndex}_shell`];
      const rocketBody = rocketEntity.bodies[`stage${stageIndex}_shell`];
      if (stageIndex == 0) {
        rocketMesh.position.y = stage.shell.height / 2;
        rocketBody.position.y = stage.shell.height / 2;
      }
      else {
        rocketMesh.position.y = rocketEntity.meshes[`stage${stageIndex-1}_shell`].position.y + rocketEntity.state.stages[stageIndex-1].shell.height/2 + stage.shell.height/2;
        rocketBody.position.y = rocketEntity.bodies[`stage${stageIndex-1}_shell`].position.y + rocketEntity.state.stages[stageIndex-1].shell.height/2 + stage.shell.height/2;
      }
  
      rocketBody.collisionFilterGroup = COLLISION_GROUP_ENGINE;

      const engineLayout = stage.engineLayout;
      engineLayout.forEach((layer, layerIndex) => {
        layer.positions.forEach((position, engineIndex) => {
          const engineBody = rocketEntity.bodies[`stage${stageIndex}_layer${layerIndex}_engine${engineIndex}`];
          const engineMesh = rocketEntity.meshes[`stage${stageIndex}_layer${layerIndex}_engine${engineIndex}`];
          
          engineMesh.position.y = rocketBody.position.y - stage.shell.height/2;
          engineBody.position.y = rocketBody.position.y - stage.shell.height/2;
          
          engineBody.collisionFilterGroup = COLLISION_GROUP_ENGINE;
          engineBody.collisionFilterMask = 0;
  
          const localShellPoint = new CANNON.Vec3(position.x, -stage.shell.height / 2, position.y);
          const localEnginePoint = new CANNON.Vec3(0, 0, 0);
  
          const constraint = new CANNON.PointToPointConstraint(rocketBody, localShellPoint, engineBody, localEnginePoint);
          cosm.physicsWorld.addConstraint(constraint);
        });
      });
    });

    this.lockStages(rocketEntity, cosm);
    // this.translateRocket(rocketEntity, { x: 0, y: rocketEntity.state.stages[0].shell.height / 2, z: 0 });
  
    console.log("Rocket created:", this);
  },

  lockStages(rocketEntity, cosm) {
    rocketEntity.locks = {};
    rocketEntity.state.stages.forEach((stage, stageIndex) => {
      if (stageIndex > 0) {
        const prevStage = rocketEntity.state.stages[stageIndex - 1];
        const shellRadius = stage.shell.radius;
  
        const pointsPrevStage = [
          new CANNON.Vec3(Math.min(shellRadius,prevStage.shell.radius), prevStage.shell.height / 2, 0),
          new CANNON.Vec3(-Math.min(shellRadius,prevStage.shell.radius), prevStage.shell.height / 2, 0),
          new CANNON.Vec3(0, prevStage.shell.height / 2, Math.min(shellRadius,prevStage.shell.radius)),
          new CANNON.Vec3(0, prevStage.shell.height / 2, -Math.min(shellRadius,prevStage.shell.radius)),
        ];

        const pointsCurrStage = [
          new CANNON.Vec3(pointsPrevStage[0].x, -stage.shell.height / 2, pointsPrevStage[0].z),
          new CANNON.Vec3(pointsPrevStage[1].x, -stage.shell.height / 2, pointsPrevStage[1].z),
          new CANNON.Vec3(pointsPrevStage[2].x, -stage.shell.height / 2, pointsPrevStage[2].z),
          new CANNON.Vec3(pointsPrevStage[3].x, -stage.shell.height / 2, pointsPrevStage[3].z),
        ];
  
        pointsPrevStage.forEach((point, lockIndex) => {
          const lockID = `stage${stageIndex}_lock${lockIndex}`;
          const constraint = new CANNON.PointToPointConstraint(
            rocketEntity.bodies[`stage${stageIndex - 1}_shell`], point,
            rocketEntity.bodies[`stage${stageIndex}_shell`], pointsCurrStage[lockIndex]
          );
          cosm.physicsWorld.addConstraint(constraint);
          rocketEntity.locks[lockID] = constraint;
        });
      }
    });
  },
  
  unlockStages(rocketEntity, cosm) {
    // Remove all stage locking constraints
    Object.values(rocketEntity.locks).forEach((constraint) => {
      cosm.physicsWorld.removeConstraint(constraint);
    });
    rocketEntity.locks = {}; // Clear the locks object
  },

  translateRocket(rocketEntity, translation) {
    Object.values(rocketEntity.meshes).forEach(mesh => {
      mesh.position.x += translation.x;
      mesh.position.y += translation.y;
      mesh.position.z += translation.z;
    });
    
    Object.values(rocketEntity.bodies).forEach(body => {
      body.position.x += translation.x;
      body.position.y += translation.y;
      body.position.z += translation.z;
      // Depending on your physics engine, you might need to explicitly update the body's position
      // For example, if you're using Cannon.js:
      body.quaternion.w = 1; // Reset quaternion to avoid rotation during translation
      body.aabbNeedsUpdate = true; // Flag to update the body's bounding box
      body.updateMassProperties(); // Recalculate mass properties based on new position
    });
  },

  generateThrottleMessage(stageId, configs, rocketEntity) {
    return {
      type: 'setThrottle',
      content: {
        stageId: stageId,
        configs: configs
      },
      recipientId: rocketEntity.id
    };
  },

  setThrottle(message, rocketEntity) {
    // console.log("[REACTION] Setting Throttle");
    
    const stageId = message.content.stageId;
    const configs = message.content.configs;
    const stage = rocketEntity.state.stages[stageId];
    // console.log(configs);
    
    if (!stage) {
      console.warn(`Stage with ID ${stageId} not found`);
      return;
    }
    
    configs.forEach((config) => {
      if (stage.engines[config.index] !== undefined) {
        stage.engines[config.index].throttle = config.throttle;
      } else {
        console.warn(`Engine with index ${engineIndex} not found in stage ${stageId}`);
      }
    });
  }
};