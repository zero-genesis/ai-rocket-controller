import * as THREE from 'three';
import * as CANNON from 'cannon-es';
import { CSS2DObject } from 'three/examples/jsm/renderers/CSS2DRenderer.js';

export function createLabel(name, mesh, labelId, relativePosition=[0,1,0]) {
    // Compute the bounding sphere if it's not already present
    if (!mesh.geometry.boundingSphere) {
        mesh.geometry.computeBoundingSphere();
    }

    const labelDiv = document.createElement('div');
    labelDiv.id = labelId;
    labelDiv.className = 'label';
    labelDiv.textContent = name;
    labelDiv.style.color = 'white';
    labelDiv.style.marginTop = '-1em';

    const label = new CSS2DObject(labelDiv);
    label.position.set(mesh.geometry.boundingSphere.radius * relativePosition[0], mesh.geometry.boundingSphere.radius * relativePosition[1], mesh.geometry.boundingSphere.radius * relativePosition[2]);
    mesh.add(label);

    console.log("Created label: ", mesh.geometry.boundingSphere.radius);
    return label;
}


export function removeLabel(mesh, label) {
    mesh.remove(label);
    label.element.parentNode.removeChild(label.element);
}

export function convertGeoTo3DVector(latitude, longitude, planetRadius, altitude) {
    const phi = (90 - latitude) * (Math.PI / 180);
    const theta = (longitude + 180) * (Math.PI / 180);

    const x = (planetRadius + altitude) * Math.sin(phi) * Math.cos(theta);
    const z = (planetRadius + altitude) * Math.sin(phi) * Math.sin(theta); // Y becomes Z
    const y = (planetRadius + altitude) * Math.cos(phi); // Z becomes Y

    return new THREE.Vector3(x, y, z);
}

export function convert3DVectorToGeo(vector, planetRadius) {
    const x = vector.x;
    const y = vector.y;
    const z = vector.z;
    const distance_xyz = Math.sqrt(x * x + y * y + z * z);
    const distance_xz = Math.sqrt(x * x + z * z);

    const latitude = Math.atan(y / distance_xz) * (180 / Math.PI);
    const longitude = (Math.atan(z / x) * (180 / Math.PI)) % 360 - 180;
    const altitude = distance_xyz - planetRadius;

    return {latitude, longitude, altitude};
}

export function getSurfaceNormalQuaternion(latitude, longitude, planetRadius) {
    // Convert latitude and longitude to 3D position
    const position = convertGeoTo3DVector(latitude, longitude, planetRadius, 0);

    // Calculate the normal at this point by simply normalizing the position vector
    // (since we're assuming a perfect sphere, the normal is the position vector normalized)
    const normal = position.clone().normalize();

    // Assuming the rocket's 'up' vector should point along the positive Y-axis, 
    // calculate the quaternion that aligns the Y-axis with the normal vector
    const up = new THREE.Vector3(0, 1, 0);
    const quaternion = new THREE.Quaternion().setFromUnitVectors(up, normal);

    return quaternion;
}

export function calculateRelativeOffsets(objects, anchorIndex) {
    const anchorPosition = objects[anchorIndex] instanceof THREE.Object3D
                           ? objects[anchorIndex].position.clone()
                           : new THREE.Vector3(objects[anchorIndex].position.x, objects[anchorIndex].position.y, objects[anchorIndex].position.z);

    return objects.map(object => {
        if (object instanceof THREE.Object3D) {
            return object.position.clone().sub(anchorPosition);
        } else { // CANNON.Body
            return new THREE.Vector3(object.position.x, object.position.y, object.position.z).sub(anchorPosition);
        }
    });
}

export function translateObjects(objects, newPosition) {
    objects.forEach((object) => {
        object.position.set(newPosition.x, newPosition.y, newPosition.z);
    });
}

export function relativeTranslateObjects(objects, newPosition, anchorIndex = 0) {
    anchorIndex = Math.min(objects.length - 1, Math.max(0, anchorIndex));

    const relativeOffsets = calculateRelativeOffsets(objects, anchorIndex);

    objects.forEach((object, index) => {
        const position = new THREE.Vector3(newPosition.x + relativeOffsets[index].x, newPosition.y + relativeOffsets[index].y, newPosition.z + relativeOffsets[index].z);
        translateObjects([object], position);
    });
}

export function rotateAroundObject(objects, quaternion, anchorIndex = 0) {
    const anchorPosition = new THREE.Vector3(objects[anchorIndex].position.x, objects[anchorIndex].position.y, objects[anchorIndex].position.z);
    anchorIndex = Math.min(objects.length - 1, Math.max(0, anchorIndex));

    const relativeOffsets = calculateRelativeOffsets(objects, anchorIndex);

    objects.forEach((object, index) => {
        const objectPosition = relativeOffsets[index];
        const objectQuaternion = new THREE.Quaternion(object.quaternion.x, object.quaternion.y, object.quaternion.z, object.quaternion.w);
        objectPosition.applyQuaternion(quaternion);
        objectQuaternion.multiplyQuaternions(quaternion, objectQuaternion);
        objectPosition.add(anchorPosition);
        object.position.set(objectPosition.x, objectPosition.y, objectPosition.z);
        object.quaternion.set(objectQuaternion.x, objectQuaternion.y, objectQuaternion.z, objectQuaternion.w);
    });
}

export function placeObjectsRelativeOnPlanet(objects, latitude, longitude, altitude, planetRadius, anchorIndex = 0) {
    const newPosition = convertGeoTo3DVector(latitude, longitude, planetRadius, altitude);
    const quaternion = getSurfaceNormalQuaternion(latitude, longitude, planetRadius);
    anchorIndex = Math.min(objects.length - 1, Math.max(0, anchorIndex));
    
    const relativeOffsets = calculateRelativeOffsets(objects, anchorIndex);

    relativeTranslateObjects(objects, newPosition, anchorIndex);
    rotateAroundObject(objects, quaternion, anchorIndex);
}

export default {
    createLabel,
    removeLabel,
    convertGeoTo3DVector,
    convert3DVectorToGeo,
    getSurfaceNormalQuaternion,
    calculateRelativeOffsets,
    placeObjectsRelativeOnPlanet,
    translateObjects,
    relativeTranslateObjects,
    rotateAroundObject
};
