export function applyReverseRotation(_ecef, angles) {
    let ecef = [0, 0, 0];
    ecef[0] = _ecef["x"];
    ecef[1] = _ecef["y"];
    ecef[2] = _ecef["z"];
    
    let theta1 = angles[0];
    let theta2 = angles[1];
    
    let x0 = ecef[0];
    let z0 = ecef[2];
    ecef[0] = Math.cos(theta2) * x0 - Math.sin(theta2) * z0;
    ecef[2] = Math.sin(theta2) * x0 + Math.cos(theta2) * z0;

    let xt = ecef[0];
    let y0 = ecef[1];
    ecef[0] = Math.cos(theta1) * xt + Math.sin(theta1) * y0;
    ecef[1] = -Math.sin(theta1) * xt + Math.cos(theta1) * y0;

    return ecef;
}