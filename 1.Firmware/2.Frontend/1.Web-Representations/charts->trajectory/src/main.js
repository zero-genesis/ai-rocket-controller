import { createApp } from 'vue'
import App from './App.vue'

import ElementPlus from 'element-plus'

import './style.css'
import 'element-plus/dist/index.css'
import 'element-plus/theme-chalk/dark/css-vars.css'
import './el-style.css'

import Trajectory from './components/Trajectory.vue'

const queryString = window.location.href;
const parts = queryString.split('/');
const axis = parts[parts.length - 1];
console.log(axis);
var axisNumber;

if (axis.toLowerCase().localeCompare("x") == 0) {
    axisNumber = 0;
} else {
    axisNumber = 2;
}

const app = createApp(App, {
    axis: axisNumber
});

app.use(ElementPlus);

app.component('wdgt-trajectory', Trajectory);

app.mount('#app');
