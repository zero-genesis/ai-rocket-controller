import * as THREE from 'three';
import * as CANNON from 'cannon-es';
import CannonDebugger from 'cannon-es-debugger';
import SceneInit from './SceneInit'

// TODO: Not yet firs principal enought (add onCreate for example to create specific physics world)
export class Cosm {
  constructor(options = {}) {
    const defaults = {
      gravity: new CANNON.Vec3(0, -9.82, 0),
      fixedStepRate: 1 / 60, // 60 FPS
      onCreate: () => {},
      customPhysicsWorld: null, // Expecting a CANNON.World instance or null
      canvasElement: null,
    };

    const settings = { ...defaults, ...options };

    this.physicsWorld = settings.customPhysicsWorld || new CANNON.World({
      gravity: settings.gravity,
    });

    if (!settings.customPhysicsWorld) {
      this.physicsWorld.fixedStep(settings.fixedStepRate);
    }

    this.sceneInit = new SceneInit(settings.canvasElement);
    this.entities = [];
    this.onCreate = settings.onCreate;

    // Call the onCreate method
    this.onCreate();
  }

  mount() {
    this.sceneInit.initialize();
    this.sceneInit.animate();
    this.cannonDebugger = new CannonDebugger(this.sceneInit.scene, this.physicsWorld);
  }

  addEntity(entity) {
    this.entities.push(entity);
    entity.setCosm(this);
    entity.addToWorld(this.physicsWorld, this.sceneInit.scene);
    entity.onCreate();
  }

  // Handle messages sent by entities
  handleMessage(message, sender) {
    console.log("[COSM] Handling message: ", sender, message);
    // Redirect message based on the recipient's type or ID
    if (message.recipientType) {
      this.entities
        .filter(entity => entity.type === message.recipientType)
        .forEach(entity => entity.handleMessage(message));
    } else if (message.recipientId) {
      const recipient = this.entities.find(entity => entity.id === message.recipientId);
      if (recipient) {
        recipient.handleMessage(message);
      }
    } else {
      // Broadcast to all entities excluding the sender
      this.entities
        .filter(entity => entity !== sender)
        .forEach(entity => entity.handleMessage(message));
    }
  }

  sendMessage(message, sender) {
    // If a recipient ID is provided, send the message directly to that entity
    if (message.recipientId) {
      const recipient = this.entities.find(entity => entity.id === message.recipientId);
      if (recipient) {
        recipient.handleMessage(message);
      }
    }
    // If a recipient type is provided, send the message to all entities of that type
    else if (message.recipientType) {
      this.entities
        .filter(entity => entity.type === message.recipientType)
        .forEach(entity => entity.handleMessage(message));
    }
    // If no recipient is provided, broadcast the message to all entities (excluding the sender if provided)
    else {
      this.entities
        .filter(entity => !sender || entity !== sender)
        .forEach(entity => entity.handleMessage(message));
    }
  }

  animate() {
    const animate = () => {
      // console.log("Updating Physics World");
      this.physicsWorld.fixedStep();
      this.cannonDebugger.update();
      this.entities.forEach(entity => entity.update());
      this.sceneInit.animate();
      window.requestAnimationFrame(animate);
    };
    animate();
  }
}

// TODO: Not fully first principal
export class Entity {
  constructor(options) {
    this.id = options.id || this.generateId();
    this.type = options.type || 'generic';
    this.state = options.state || {};
    this.meshes = options.meshes || {};
    this.bodies = options.bodies || {};
    console.log(options.reactions);
    this.reactions = options.reactions || {};
    console.log(this.reactions);
    this.onCreate = options.onCreate || (() => {});
    this.updateFunction = options.update || this.defaultUpdate;
  }

  // Add the entity's bodies and meshes to the physics world and scene
  addToWorld(physicsWorld, scene) {
    Object.values(this.bodies).forEach(body => physicsWorld.addBody(body));
    Object.values(this.meshes).forEach(mesh => scene.add(mesh));
    Object.entries(this.bodies).forEach(([key, body]) => {
      if (this.meshes[key]) this.syncMeshWithBody(this.meshes[key], body);
    });

  }

  // Default update function for entities
  defaultUpdate() {
    Object.entries(this.bodies).forEach(([key, body]) => {
      if (this.meshes[key]) this.syncMeshWithBody(this.meshes[key], body);
    });
  }

  // Update function that gets called on every frame
  update() {
    this.updateFunction();
  }

  // Sync a Three.js mesh with a Cannon.js body
  syncMeshWithBody(mesh, body) {
    mesh.position.copy(body.position);
    mesh.quaternion.copy(body.quaternion);
    // console.log("Synchronizing Mesh with Body:", mesh.position, mesh.quaternion);
  }

  // Apply a force to an entity's body
  applyForce(force, point, bodyName = 'default') {
    if (this.bodies[bodyName]) {
      this.bodies[bodyName].applyLocalForce(force, point);
    }
  }

  // Set the cosm for this entity
  setCosm(cosm) {
    this.cosm = cosm;
  }

  // Send a message through the cosm
  sendMessage(message) {
    console.log("Sending message", message);
    if (this.cosm) {
        this.cosm.handleMessage(message, this);
    } else {
        console.error('This entity is not attached to a cosm.');
    }
  }

  // Handle messages received by the entity
  handleMessage(message) {
    console.log("[ENTITY] Handling message", this.id, message, this.reactions);
    if (this.reactions[message.type]) {
      console.log("[ENTITY] Found reaction", message, this.reactions);
      this.reactions[message.type].forEach(reaction => {
        reaction.handler.call(this, message);
      });
    }
  }

  // Generate a unique ID for the entity
  generateId() {
    return '_' + Math.random().toString(36);
  }
}

export default { Cosm, Entity };