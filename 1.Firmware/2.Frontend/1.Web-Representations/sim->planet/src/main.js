import { createApp } from 'vue'
import App from './App.vue'

import ElementPlus from 'element-plus'

import './style.css'
import 'element-plus/dist/index.css'
import 'element-plus/theme-chalk/dark/css-vars.css'
import './el-style.css'

import Planet from './components/Planet.vue'

const app = createApp(App);

app.use(ElementPlus);

app.component('wdgt-planet', Planet);

app.mount('#app');