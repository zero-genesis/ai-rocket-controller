import { createApp } from 'vue';
import App from './App.vue';
import { createRouter, createWebHistory } from 'vue-router';

import ElementPlus from 'element-plus';
import './style.css';
import 'element-plus/dist/index.css';
import 'element-plus/theme-chalk/dark/css-vars.css';
import './el-style.css';

import AltitudeComponent from './components/AltitudeComponent.vue';
import EarthComponent from './components/EarthComponent.vue';

// Define routes
const routes = [
    { path: '/altitude', component: AltitudeComponent },
    { path: '/earth', component: EarthComponent }
];

// Create the router instance
const router = createRouter({
    history: createWebHistory('/wdgt/maps'),
    routes
});

const app = createApp(App);

app.use(ElementPlus);
app.use(router); // Use the router instance

app.mount('#app');
