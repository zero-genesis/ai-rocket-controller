import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import path from 'path'


// https://vitejs.dev/config/
export default defineConfig({
  base:'/wdgt/maps',
  plugins: [vue()],
  resolve: {
    alias: {
      // Add your aliases here
      '/wdgt/maps/assets': path.resolve(__dirname, './public/assets')
    }
  }
})
