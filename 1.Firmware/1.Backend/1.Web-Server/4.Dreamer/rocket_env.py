import pymunk
import gym as gym
import numpy as np
import cv2
import math

render_flag = False
class RocketEnv(gym.Env):
    def __init__(self):
        self.actions = {
            0: "UP",
            1: "UP+RIGHT",
            2: "UP+LEFT",
            3: "OFF",
        }

        self.fps = 120
        self.dt = 1/self.fps
        self.max_simulation_duration = (5*60)
        self.initial_step_count = 2000
        self.max_step_count = self.initial_step_count

        self.space = pymunk.Space()
        self.space.gravity = (0, 9.81)

        self.width = 256
        self.height = 256
        self._size = (self.width, self.height)
        self._obs_size = (64, 64)

        self.rocket_config = {
            "height": 70,
            "diameter": 3.7,
            "mass": 549054 * 0.75,
            "thrust": 7607000,
        }
        self.ground_config = {
            "width": self.width*100,
            "height": 20,
        }

        self.previous_rocket_position = (0, 0)
        self.previous_distance_to_checkpoint = 0
        self.step_count = 0
        self.previous_step_count = 0
        self.current_checkpoint_idx = 0

        if render_flag:
            cv2.namedWindow("My Canvas", cv2.WINDOW_NORMAL)

        self.reset()
    
    @property
    def observation_space(self):
        return gym.spaces.Box(0, 255, tuple(self._obs_size) + (3,), np.uint8)

    @property
    def action_space(self):
        return gym.spaces.Discrete(len(self.actions))

    def _create_rectangle(self, space, width, height, mass, position, angle=0, body_type=pymunk.Body.DYNAMIC, color=(255, 0, 0, 100), custom_fn=lambda space, body, shape: None):
        body = pymunk.Body(body_type=body_type)
        body.position = position
        shape = pymunk.Poly.create_box(body, (width, height))
        shape.mass = mass
        shape.angle = angle
        shape.color = color
        custom_fn(space, body, shape)
        space.add(body, shape)
        return shape

    def _rocket_init(self, space, body, shape):
        shape.elasticity = 0.4
        shape.friction = 0.7

    def _wall_init(self, space, body, shape):
        shape.elasticity = 0.4
        shape.friction = 0.5

    def _trajectory_function(self, altitude, max_altitude, scale, component=False):
        side = (-1) if not component else 1
        def base_function(altitude, max_altitude, scale, side):
            return side*scale*math.sqrt(max_altitude-altitude)
        
        return (base_function(altitude, max_altitude, scale, side) - base_function(0, max_altitude, scale, (-1)), altitude)

    def _generate_trajectory_checkpoints(self, start_altitude, end_altitude, interval, scale, component=False):
        trajectory = [self._trajectory_function(altitude, end_altitude, scale, component) for altitude in range(start_altitude, end_altitude+1, interval)]
        trajectory = [(checkpoint[0], -checkpoint[1]) for checkpoint in trajectory]
        return trajectory
    
    def reset(self):
        self.step_count = 0
        self.max_step_count = self.initial_step_count
        self.previous_step_count = 0
        self.current_checkpoint_idx = 0
        self.canvas = np.zeros(tuple(self._size) + (3,), np.uint8)
        self.rocket = self._create_rectangle(self.space, self.rocket_config["diameter"], self.rocket_config["height"], self.rocket_config["mass"], (self.width/2, self.height-20//2-self.rocket_config["height"]//2), body_type=pymunk.Body.DYNAMIC, color=(255, 0, 0, 100), custom_fn=self._rocket_init)
        self.ground = self._create_rectangle(self.space, self.width*100, 20, 0, (self.width*100/2, self.height-20//2), body_type=pymunk.Body.STATIC, color=(255, 255, 255, 100), custom_fn=self._wall_init)
        self.previous_rocket_position = (self.rocket.body.position[0], self.rocket.body.position[1])
        self.trajectory_checkpoints = [(checkpoint[0] + self.rocket.body.position[0], checkpoint[1] + self.rocket.body.position[1]) for checkpoint in self._generate_trajectory_checkpoints(300, 13000, 300, 80)]
        
        checkpoint = self.trajectory_checkpoints[0]
        self.minimum_distance = math.sqrt((checkpoint[0] - self.rocket.body.position[0])**2 + (checkpoint[1] - self.rocket.body.position[1])**2)
        self.previous_distance_to_checkpoint = self.minimum_distance
        self.previous_speed = 0

        return self._obs()

    def render(self):
        if render_flag:
            cv2.imshow("My Canvas", self.canvas)
            cv2.waitKey(int(1/self.fps*1000))

    def _rocket_center(self, space, width, height, rocket_position, checkpoints):
        for body in space.bodies:
            body.position = body.position.x - rocket_position.x + width / 2, body.position.y - rocket_position.y + height / 2
            space.reindex_shapes_for_body(body)
    
        for i in range(0, len(checkpoints)):
            checkpoints[i] = (checkpoints[i][0] - rocket_position.x + width / 2, checkpoints[i][1] - rocket_position.y + height / 2)

    def _reached_checkpoint(self):
        checkpoint = self.trajectory_checkpoints[self.current_checkpoint_idx]
        distance = math.sqrt((self.rocket.body.position.x - checkpoint[0])**2 + (self.rocket.body.position.y - checkpoint[1])**2)
        return distance <= 2 * self.rocket_config["height"]

    def _draw(self, custom_fn=lambda: None):
        self.canvas.fill(0)
        custom_fn()
        
    def _draw_checkpoint_indicators(self):
        colors = [(255, 255, 0), (0, 255, 255), (255, 0, 255)]
        for i in range(2, -1, -1):
            if not self.current_checkpoint_idx + i < len(self.trajectory_checkpoints): continue
            checkpoint = self.trajectory_checkpoints[self.current_checkpoint_idx + i]
            direction = (checkpoint[0] - self.rocket.body.position[0], checkpoint[1] - self.rocket.body.position[1])
            direction_norm = math.sqrt(direction[0]**2 + direction[1]**2)
            target_rel = (direction[0]/direction_norm*self.rocket_config["height"], direction[1]/direction_norm*self.rocket_config["height"])

            target_abs = (self.rocket.body.position[0] + target_rel[0], self.rocket.body.position[1] + target_rel[1])

            shift = 2.5
            scalar = 1000
            radius = 1/(4*math.pi*math.sqrt(direction_norm+shift)) * scalar + self.rocket_config["height"] * 0.025
            # radius = ((-direction_norm + 500) / 500)*5
            cv2.circle(self.canvas, (int(target_abs[0]), int(target_abs[1])), max(1, int(radius)), colors[i], 2, cv2.LINE_AA)

    def _draw_direction(self):
        # Center of the rocket
        rocket_center_x = self.rocket.body.position.x
        rocket_center_y = self.rocket.body.position.y

        # Offset to the top of the rocket from its center
        offset_x = (self.rocket_config["height"] / 2) * math.sin(self.rocket.body.angle)
        offset_y = (self.rocket_config["height"] / 2) * math.cos(self.rocket.body.angle)

        # Top point of the rocket
        rocket_top_x = rocket_center_x + offset_x
        rocket_top_y = rocket_center_y - offset_y

        line_length = 20  # Length of the direction indicator line

        # Calculate the end point of the line based on the rocket's angle
        end_x = rocket_top_x + line_length * math.sin(self.rocket.body.angle)
        end_y = rocket_top_y - line_length * math.cos(self.rocket.body.angle)

        cv2.line(self.canvas, (int(rocket_top_x), int(rocket_top_y)), (int(end_x), int(end_y)), (0, 255, 0), 2, cv2.LINE_AA)

    def _draw_checkpoints(self):
        for checkpoint in self.trajectory_checkpoints:
            cv2.circle(self.canvas, (int(checkpoint[0]), int(checkpoint[1])), 5, (255, 255, 255), -1, cv2.LINE_AA)

    def _draw_rectangle(self, rectangle, width, height, color=(255, 255, 255)):
        # Get the rocket's center position
        center_x, center_y = int(rectangle.body.position.x), int(rectangle.body.position.y)
        
        # Calculate half dimensions
        half_width = width / 2
        half_height = height / 2

        # Calculate the angle of the rocket
        angle = rectangle.body.angle

        # Calculate the corners of the rectangle
        corners = []
        for dx, dy in [(-half_width, -half_height), (-half_width, half_height), (half_width, half_height), (half_width, -half_height)]:
            rx = dx * math.cos(angle) - dy * math.sin(angle)
            ry = dx * math.sin(angle) + dy * math.cos(angle)
            corners.append((center_x + rx, center_y + ry))

        # Convert corners to a format suitable for cv2.polylines
        pts = np.array(corners, np.int32)
        pts = pts.reshape((-1, 1, 2))

        # Draw the rectangle
        cv2.fillPoly(self.canvas, [pts], color=color, lineType=cv2.LINE_AA)

    def _draw_rocket(self):
        self._draw_rectangle(self.rocket, self.rocket_config["diameter"], self.rocket_config["height"], (0, 0, 255))

    def _draw_ground(self):
        self._draw_rectangle(self.ground, self.ground_config["width"], self.ground_config["height"], (255, 0, 0))

    def _custom_draw(self):
        self._draw_checkpoint_indicators()
        self._draw_direction()
        self._draw_checkpoints()
        self._draw_rocket()
        self._draw_ground()
    
    def _obs(self):
        scaled_canvas = cv2.resize(self.canvas, self._obs_size, interpolation=cv2.INTER_AREA)
        return scaled_canvas
    
    def _distance_reward(self):
        current_distance_to_checkpoint = math.sqrt((self.trajectory_checkpoints[self.current_checkpoint_idx][0] - self.rocket.body.position[0])**2+\
                                                    (self.trajectory_checkpoints[self.current_checkpoint_idx][1] - self.rocket.body.position[1])**2)

        # Reward calculation
        speed = math.sqrt((self.rocket.body.position[0] - self.previous_rocket_position[0])**2 + (self.rocket.body.position[1] - self.previous_rocket_position[1])**2)
        steps = current_distance_to_checkpoint/max(speed, 1e-9)
        reward = (self.previous_distance_to_checkpoint/current_distance_to_checkpoint)/steps * 100

        self.previous_distance_to_checkpoint = current_distance_to_checkpoint
        
        return reward

    def _checkpoint_reward(self):
        reward = 0
        if self.current_checkpoint_idx > 0:
            reward_constant = 100
            estimated_rocket_speed = (self.previous_rocket_position[0] - self.rocket.body.position[0], self.previous_rocket_position[1] - self.rocket.body.position[1])
            estimated_rocket_speed = math.sqrt(estimated_rocket_speed[0]**2 + estimated_rocket_speed[1]**2)
            distance_travelled = (self.trajectory_checkpoints[self.current_checkpoint_idx][0] - self.trajectory_checkpoints[self.current_checkpoint_idx-1][0],\
                                self.trajectory_checkpoints[self.current_checkpoint_idx][1] - self.trajectory_checkpoints[self.current_checkpoint_idx-1][1])
            distance_travelled = math.sqrt(distance_travelled[0]**2 + distance_travelled[1]**2)
            optimal_travel_duration = distance_travelled/max(estimated_rocket_speed, 1e-9)
            real_travel_duration = self.step_count - self.previous_step_count

            reward = optimal_travel_duration/real_travel_duration * reward_constant
            self.previous_step_count = self.step_count

        return reward
    
    def _initial_checkpoint_reward(self):
        reward = 0
        self._update_minimum_distance()
        if self.step_count == 1000:
            distance_scalar = 1/10
            reward = (1 / (self.minimum_distance*distance_scalar + 1)) * 100
        
        return reward
    
    def _estimate_step_count_to_next_checkpoint(self):
        # Assuming a constant speed for estimation, you can adjust this based on your simulation's dynamics
        current_speed = math.sqrt((self.rocket.body.position[0] - self.previous_rocket_position[0])**2 + (self.rocket.body.position[1] - self.previous_rocket_position[1])**2)
        estimated_speed = self.previous_speed * 0.35 + current_speed * 0.65
        estimated_steps = 0

        if self.current_checkpoint_idx < len(self.trajectory_checkpoints) - 1:
            next_checkpoint = self.trajectory_checkpoints[self.current_checkpoint_idx + 1]
            distance_to_next_checkpoint = math.sqrt((next_checkpoint[0] - self.rocket.body.position[0])**2 + (next_checkpoint[1] - self.rocket.body.position[1])**2)
            estimated_steps = distance_to_next_checkpoint / max(estimated_speed, 1e-9)
            estimated_steps = min(estimated_steps, self.initial_step_count)

        self.previous_speed = current_speed
        return estimated_steps

    def step(self, action):
        terminated = False
        reached_checkpoint_flag = False
        
        self.step_count+=1
        
        thrust_x = 0
        thrust_y = 0

        if action == 0:
            thrust_y = -1
        elif action == 1:
            thrust_y = -1
            thrust_x = 0.5
        elif action == 2:
            thrust_y = -1
            thrust_x = -0.5
        elif action == 3:
            pass

        # [START] Simulation
        magnitude = math.sqrt(thrust_x**2 + thrust_y**2)

        if magnitude > 0:
            thrust_x = (thrust_x / magnitude) * self.rocket_config["thrust"]
            thrust_y = (thrust_y / magnitude) * self.rocket_config["thrust"]
            self.rocket.body.apply_force_at_local_point((thrust_x, thrust_y), (0, self.rocket_config["height"] / 2))
        
        self.space.step(self.dt)
        # [END] Simulation

        # [START] Flags and simulation state
        if self._reached_checkpoint():
            reached_checkpoint_flag = True
            additional_steps = self._estimate_step_count_to_next_checkpoint()
            self.max_step_count += additional_steps

        if self.step_count == self.max_step_count:
            terminated = True
        # [END] Flags and simulation state

        rewards = [
            self._distance_reward() if not reached_checkpoint_flag else 0,
            self._initial_checkpoint_reward() if self.current_checkpoint_idx == 0 else 0,
            self._checkpoint_reward() if reached_checkpoint_flag else 0,
        ]

        info = {
            "position": self.rocket.body.position,
            "next_checkpoint": self.current_checkpoint_idx,
            "reward": sum(rewards),
            "rewards": rewards,
        }
        
        # [START] Finishing up
        if reached_checkpoint_flag:
            self.current_checkpoint_idx += 1
            if self.current_checkpoint_idx == len(self.trajectory_checkpoints): terminated = True
            self.previous_distance_to_checkpoint = math.sqrt((self.trajectory_checkpoints[self.current_checkpoint_idx][0] - self.rocket.body.position[0])**2+\
                                                    (self.trajectory_checkpoints[self.current_checkpoint_idx][1] - self.rocket.body.position[1])**2)

        self.previous_rocket_position = (self.rocket.body.position[0], self.rocket.body.position[1])
        self._rocket_center(self.space, self.width, self.height, self.rocket.body.position, self.trajectory_checkpoints)
        self._draw(custom_fn=self._custom_draw)
        # [END] Finishing up
        
        return self._obs(), sum(rewards), terminated, info
    
    def _update_minimum_distance(self):
        self.minimum_distance = min(self.previous_distance_to_checkpoint, self.minimum_distance)

    def close(self):
        if render_flag:
            cv2.destroyAllWindows()