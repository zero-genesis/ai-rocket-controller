#!/bin/sh
git clone https://github.com/danijar/dreamerv3.git

sudo apt-get update && sudo apt-get install -y \
  ffmpeg git python3-pip vim libglew-dev \
  x11-xserver-utils xvfb \
  && sudo apt-get clean

pip3 install crafter
pip3 install dm_control
pip3 install robodesk
pip3 install bsuite

pip3 install jax[cuda11_pip] -f https://storage.googleapis.com/jax-releases/jax_cuda_releases.html
# pip3 install jaxlib
pip3 install tensorflow_probability
pip3 install optax
pip3 install tensorflow[and-cuda]
# pip3 install tensorflow-cpu

pip3 install numpy cloudpickle ruamel.yaml rich zmq msgpack pymunk opencv-python