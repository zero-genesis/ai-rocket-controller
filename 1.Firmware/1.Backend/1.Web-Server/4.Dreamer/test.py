from rocket_env import RocketEnv

env = RocketEnv()
env.reset()
truncated = 0
while not truncated:
    _, _, _, truncated, _ = env.step(0)
    env.render()

print("Finished")
    