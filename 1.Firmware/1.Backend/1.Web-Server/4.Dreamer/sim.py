import pymunk
import math
import cv2
import numpy as np
from pynput import keyboard

width, height = 256, 256
observation_width, observation_height = 128, 128
render_flag = True
key_states = {
    "Key.up": False,
    "Key.left": False,
    "Key.right": False,
    "Key.ctrl": False,
}

def on_press(key):
    try:
        key_str = str(key)
        if key_str in key_states:
            print(f"Pressed Detected {key}")
            key_states[key_str] = True
    except AttributeError:
        print(f"Pressed ERROR {key}")

def on_release(key):
    try:
        print(f"Released {key}")
        key_str = str(key)
        if key_str in key_states:
            key_states[key_str] = False
    except AttributeError:
        print(f"Released ERROR {key}")

    if key == keyboard.Key.esc:
        return False

# Collect events until released
listener = keyboard.Listener(on_press=on_press, on_release=on_release)

def calculate_distance(point1, point2):
    return math.sqrt((point1[0] - point2[0])**2 + (point1[1] - point2[1])**2)

def calculate_angle(point1, point2):
    return math.atan2(point2[1] - point1[1], point2[0] - point1[0])

def trajectory_function(altitude, max_altitude, scale, component=False):
    side = (-1) if not component else 1
    def base_function(altitude, max_altitude, scale, side):
        return side*scale*math.sqrt(max_altitude-altitude)
    
    return (base_function(altitude, max_altitude, scale, side) - base_function(0, max_altitude, scale, (-1)), altitude)

def generate_trajectory_checkpoints(start_altitude, end_altitude, interval, scale, component=False):
    trajectory = [trajectory_function(altitude, end_altitude, scale, component) for altitude in range(start_altitude, end_altitude+1, interval)]
    trajectory = [(checkpoint[0], -checkpoint[1]) for checkpoint in trajectory]
    return trajectory

def rocket_center(space, width, height, rocket_position, checkpoints):
    for body in space.bodies:
        body.position = body.position.x - rocket_position.x + width / 2, body.position.y - rocket_position.y + height / 2
        space.reindex_shapes_for_body(body)
    
    for i in range(0, len(checkpoints)):
        checkpoints[i] = (checkpoints[i][0] - rocket_position.x + width / 2, checkpoints[i][1] - rocket_position.y + height / 2)

def draw(canvas, custom_fn=lambda: None):
    canvas.fill(0)
    custom_fn()
    if render_flag:
        scaled_canvas = cv2.resize(canvas, (observation_width, observation_height), interpolation=cv2.INTER_AREA)
        cv2.imshow("My Canvas", scaled_canvas)

def create_ball(space, radius, mass, position, body_type=pymunk.Body.DYNAMIC, color=(255, 0, 0, 100), custom_fn=lambda space, body, shape: None):
    body = pymunk.Body(body_type=body_type)
    body.position = position
    shape = pymunk.Circle(body, radius)
    shape.mass = mass
    shape.color = color
    custom_fn(space, body, shape)
    space.add(body, shape)
    return shape

def create_rectangle(space, width, height, mass, position, angle=0, body_type=pymunk.Body.DYNAMIC, color=(255, 0, 0, 100), custom_fn=lambda space, body, shape: None):
    body = pymunk.Body(body_type=body_type)
    body.position = position
    shape = pymunk.Poly.create_box(body, (width, height))
    shape.mass = mass
    shape.angle = angle
    shape.color = color
    custom_fn(space, body, shape)
    space.add(body, shape)
    return shape

def reached_checkpoint(rocket_config, rocket, checkpoint):
    distance = math.sqrt((rocket.body.position.x - checkpoint[0])**2 + (rocket.body.position.y - checkpoint[1])**2)
    return distance <= 2 * rocket_config["height"]

def run(width, height):
    if render_flag:
        cv2.namedWindow("My Canvas", cv2.WINDOW_NORMAL)
    canvas = np.zeros((height, width, 3), dtype=np.uint8)

    run = True
    fps = 120
    dt = 1/fps

    space = pymunk.Space()
    space.gravity = (0, 9.81)

    def rocket_init(space, body, shape):
        shape.elasticity = 0.4
        shape.friction = 0.7
    
    def wall_init(space, body, shape):
        shape.elasticity = 0.4
        shape.friction = 0.5

    rocket_config = {
        "height": 70,
        "diameter": 3.7,
        "mass": 549054*0.75,
        "thrust": 7607000,
    }

    ground_config = {
        "width": width*100,
        "height": 20,
    }

    rocket = create_rectangle(space, rocket_config["diameter"], rocket_config["height"], rocket_config["mass"], (width/2, height-20//2-rocket_config["height"]//2), body_type=pymunk.Body.DYNAMIC, color=(255, 0, 0, 100), custom_fn=rocket_init)
    ground = create_rectangle(space, width*100, 20, 0, (ground_config["width"]/2, height), body_type=pymunk.Body.STATIC, color=(255, 255, 255, 100), custom_fn=wall_init)

    pressed_position = (0, 0)
    trajectory_checkpoints = [(checkpoint[0] + rocket.body.position[0], checkpoint[1] + rocket.body.position[1]) for checkpoint in generate_trajectory_checkpoints(300, 13000, 300, 80)]
    current_checkpoint_idx = 0

    def draw_direction():
        # Center of the rocket
        rocket_center_x = rocket.body.position.x
        rocket_center_y = rocket.body.position.y

        # Offset to the top of the rocket from its center
        offset_x = (rocket_config["height"] / 2) * math.sin(rocket.body.angle)
        offset_y = (rocket_config["height"] / 2) * math.cos(rocket.body.angle)

        # Top point of the rocket
        rocket_top_x = rocket_center_x + offset_x
        rocket_top_y = rocket_center_y - offset_y

        line_length = 20  # Length of the direction indicator line

        # Calculate the end point of the line based on the rocket's angle
        end_x = rocket_top_x + line_length * math.sin(rocket.body.angle)
        end_y = rocket_top_y - line_length * math.cos(rocket.body.angle)

        cv2.line(canvas, (int(rocket_top_x), int(rocket_top_y)), (int(end_x), int(end_y)), (0, 255, 0), 2, cv2.LINE_AA)
        
    def draw_checkpoints():
        for checkpoint in trajectory_checkpoints:
            cv2.circle(canvas, (int(checkpoint[0]), int(checkpoint[1])), 5, (255, 255, 255), -1, cv2.LINE_AA)
    
    def draw_rectangle(rectangle, width, height, color=(255, 255, 255)):
        # Get the rocket's center position
        center_x, center_y = int(rectangle.body.position.x), int(rectangle.body.position.y)
        
        # Calculate half dimensions
        half_width = width / 2
        half_height = height / 2

        # Calculate the angle of the rocket
        angle = rectangle.body.angle

        # Calculate the corners of the rectangle
        corners = []
        for dx, dy in [(-half_width, -half_height), (-half_width, half_height), (half_width, half_height), (half_width, -half_height)]:
            rx = dx * math.cos(angle) - dy * math.sin(angle)
            ry = dx * math.sin(angle) + dy * math.cos(angle)
            corners.append((center_x + rx, center_y + ry))

        # Convert corners to a format suitable for cv2.polylines
        pts = np.array(corners, np.int32)
        pts = pts.reshape((-1, 1, 2))

        # Draw the rectangle
        cv2.fillPoly(canvas, [pts], color=color, lineType=cv2.LINE_AA)
    
    def draw_checkpoint_indicators():
        colors = [(255, 255, 0), (0, 255, 255), (255, 0, 255)]
        for i in range(2, -1, -1):
            if not current_checkpoint_idx + i < len(trajectory_checkpoints): continue
            checkpoint = trajectory_checkpoints[current_checkpoint_idx + i]
            direction = (checkpoint[0] - rocket.body.position[0], checkpoint[1] - rocket.body.position[1])
            direction_norm = math.sqrt(direction[0]**2 + direction[1]**2)
            target_rel = (direction[0]/direction_norm*rocket_config["height"], direction[1]/direction_norm*rocket_config["height"])

            target_abs = (rocket.body.position[0] + target_rel[0], rocket.body.position[1] + target_rel[1])

            shift = 2.5
            scalar = 1000
            radius = 1/(4*math.pi*math.sqrt(direction_norm+shift)) * scalar + rocket_config["height"] * 0.025
            # radius = ((-direction_norm + 500) / 500)*5
            cv2.circle(canvas, (int(target_abs[0]), int(target_abs[1])), max(1, int(radius)), colors[i], 2, cv2.LINE_AA)

    def draw_rocket():
        draw_rectangle(rocket, rocket_config["diameter"], rocket_config["height"], (0, 0, 255))

    def draw_ground():
        draw_rectangle(ground, ground_config["width"], ground_config["height"], (255, 0, 0))

    def custom_draw():
        draw_checkpoint_indicators()
        draw_direction()
        draw_checkpoints()
        draw_rocket()
        draw_ground()
    
    while run:
        thrust_x = 0
        thrust_y = 0

        if key_states["Key.up"]:
            thrust_y -= 1
        if key_states["Key.right"]:
            thrust_x -= 0.5
        if key_states["Key.left"]:
            thrust_x += 0.5

        # Calculate the magnitude of the thrust vector
        magnitude = math.sqrt(thrust_x**2 + thrust_y**2)

        if magnitude > 0:
            thrust_x = (thrust_x / magnitude) * rocket_config["thrust"]
            thrust_y = (thrust_y / magnitude) * rocket_config["thrust"]
            rocket.body.apply_force_at_local_point((thrust_x, thrust_y), (0, rocket_config["height"] / 2))

        # TODO: quit
        
        rocket_center(space, width, height, rocket.body.position, trajectory_checkpoints)
        if reached_checkpoint(rocket_config, rocket, trajectory_checkpoints[current_checkpoint_idx]):
            current_checkpoint_idx += 1
        
        space.step(dt)
        draw(canvas, custom_fn=custom_draw)
        if render_flag:
            cv2.waitKey(int(1/fps*1000))
        # TODO: render
    
    # TODO: quit
    cv2.destroyAllWindows()

if __name__ == "__main__":
    listener.start()
    run(width, height)