import os
import handlers

_ABSOLUTE_PATH = os.path.abspath('.')
SERVE_DIRECTORY = os.path.join(_ABSOLUTE_PATH, "..", "..", "..", "2.Frontend", "1.Web-Representations")

PUBLIC_DIRECTORIES = {
    "sim/rocket": None,
    "db/rocket": handlers.db_rocket_handler,
    "db/ai": handlers.db_ai_handler,
    "charts/trajectory": None
}

RAW_DIRECTORIES = [
    "global",
]

ALIASES = {
}