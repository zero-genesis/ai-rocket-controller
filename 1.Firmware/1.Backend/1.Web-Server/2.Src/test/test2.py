import pygame
import random
import torch
import torch.nn as nn
import torch.optim as optim
import numpy as np
import modules
import math

# Snake Game Environment
class SnakeGame:
    def __init__(self, width=20, height=20):
        self.width = width
        self.height = height
        self.reset()

    def reset(self):
        self.snake = [(self.width // 2, self.height // 2)]
        self.prev_head = self.snake[0]
        self.direction = (0, -1)  # Start moving up
        self.food = self._place_food()
        self.done = False

    def _place_food(self):
        while True:
            pos = (random.randint(0, self.width - 1), random.randint(0, self.height - 1))
            if pos not in self.snake:
                return pos

    def step(self, action):
        dist_before = math.sqrt((self.prev_head[0] - self.food[0])**2 + (self.prev_head[1] - self.food[1])**2)

        # Map action to direction
        directions = [(0, -1), (1, 0), (0, 1), (-1, 0)]  # Up, Right, Down, Left
        self.direction = directions[action]

        # Move snake
        new_head = (self.snake[0][0] + self.direction[0], self.snake[0][1] + self.direction[1])

        dist_after = math.sqrt((new_head[0] - self.food[0])**2 + (new_head[1] - self.food[1])**2)

        # Boundary check
        if (new_head[0] < 0 or new_head[0] >= self.width or
            new_head[1] < 0 or new_head[1] >= self.height):
            # self.done = True
            new_head = (self.width//2, self.height//2)
            return 0, self.done  # Significant penalty for hitting the wall
        
        prev_head = new_head

        self.snake.insert(0, new_head)

        reward = 0  # Time wasting penalty

        # Determine reward or penalty
        if new_head == self.food:
            print("yum")
            reward += 10  # Reward for eating food
            self.food = self._place_food()
            self.snake.pop()
        else:
            self.snake.pop()
            if dist_after < dist_before:
                # print("closer")
                reward += 1  # Reward for getting closer to food
            else:
                reward -= 1 # Penalty for moving away

        # Check if the snake hits itself (Optional)
        # if new_head in self.snake[1:]:
        #     self.done = True
        #     reward = -10  # Penalty for hitting itself

        return reward, self.done

    def get_state(self):
        # Create a binary grid representing the state
        head_x, head_y = self.snake[0]
        food_x, food_y = self.food
        state = [
            food_x,  # Difference in x-coordinate
            food_y,   # Difference in y-coordinate
            head_x,
            head_y,
            self.width,
            self.height,
        ]
        state = [
            food_x, food_y,  # Food position
            head_x, head_y,  # Head position
            head_x / self.width,  # Normalized distance to left edge
            (self.width - head_x) / self.width,  # Normalized distance to right edge
            head_y / self.height,  # Normalized distance to top edge
            (self.height - head_y) / self.height,  # Normalized distance to bottom edge
        ]
        return np.array(state, dtype=np.float32)
        state = np.zeros((self.width, self.height))
        for x, y in self.snake:
            print("snake")
            state[x, y] = 1
        state[self.food[0], self.food[1]] = 2
        return state.flatten()

# RL Agent
class SnakeAgent:
    def __init__(self, input_size, output_size, epsilon=1.0, epsilon_decay=0.995):
        self.model = nn.Sequential(
            nn.Linear(input_size, 64),
            nn.ReLU(),
            nn.Linear(64, output_size)
        )
        self.modules_dict = {
            "encoder": {
                "module": modules.FullBlock([input_size, 8, 16], activation="swish")  # 2 input features
            },
            "compute": {
                "module": modules.FullBlock([16, 16])  # 2 input features
            },
            "decoder": {
                "module": modules.FullBlock([16, 8, output_size], activation="swish")  # 2 output features
            }
        }

        self.model = modules.GeneralNetwork(self.modules_dict)

        self.optimizer = optim.Adam(self.model.parameters(), lr=0.01)
        self.epsilon = epsilon
        self.epsilon_decay = epsilon_decay

    def select_action(self, state):
        if random.random() < self.epsilon:
            return random.randint(0, 3)  # Choose a random action
        else:
            state_tensor = torch.tensor(state, dtype=torch.float).unsqueeze(0)
            with torch.no_grad():
                action_probs = self.model(state_tensor)
            return torch.argmax(action_probs).item()

    def update_epsilon(self):
        self.epsilon *= self.epsilon_decay

    def train_step(self, state, action, reward, next_state, done):
        state_tensor = torch.tensor(state, dtype=torch.float)
        next_state_tensor = torch.tensor(next_state, dtype=torch.float)
        action_tensor = torch.tensor(action, dtype=torch.long)
        reward_tensor = torch.tensor(reward, dtype=torch.float)

        # Q value of current state
        q_val = self.model(state_tensor)[action_tensor]

        # Q value of next state
        with torch.no_grad():
            q_next = self.model(next_state_tensor).max()

        # Update target
        target = reward_tensor + 0.9 * q_next * (1 - int(done))

        # Loss and backpropagation
        loss = nn.functional.mse_loss(q_val, target)
        self.optimizer.zero_grad()
        loss.backward()
        self.optimizer.step()

# Initialize game and agent
game = SnakeGame(width=20, height=20)
# agent = SnakeAgent(input_size=game.width * game.height, output_size=4)
agent = SnakeAgent(input_size=8, output_size=4, epsilon=0.25, epsilon_decay=1)  # Input size is now 2 (relative position of food)

# Pygame setup for visualization
pygame.init()
screen_size = 400
cell_size = screen_size // game.width
screen = pygame.display.set_mode((screen_size, screen_size))
clock = pygame.time.Clock()

# Training Loop
for episode in range(1000):
    state = game.get_state()
    game.reset()
    total_reward = 0

    while not game.done:
        # Select action
        action = agent.select_action(state)
        reward, done = game.step(action)
        next_state = game.get_state()
        # print(reward)

        # Train agent
        agent.train_step(state, action, reward, next_state, done)
        state = next_state
        total_reward += reward

        # if total_reward <= -1:
        #     game.done = True

        # Visualization
        screen.fill((0, 0, 0))
        for x, y in game.snake:
            pygame.draw.rect(screen, (0, 255, 0), (x * cell_size, y * cell_size, cell_size, cell_size))
        pygame.draw.rect(screen, (255, 0, 0), (game.food[0] * cell_size, game.food[1] * cell_size, cell_size, cell_size))
        pygame.display.flip()
        # clock.tick(10)

    agent.update_epsilon()
    print(f"Episode {episode}, Total Reward: {total_reward}, epsilon: {agent.epsilon}")

pygame.quit()
