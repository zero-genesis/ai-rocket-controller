import torch
import torch.optim as optim
import torch.nn.functional as F
import torch.nn as nn
import modules
import matplotlib.pyplot as plt
import numpy as np
import math

# Generate synthetic data with 2D points and categories
def generate_synthetic_data(num_samples):
    # Create two categories of 2D points
    category_1 = torch.randn(num_samples // 2, 2) + torch.tensor([2.0, 2.0])
    category_2 = torch.randn(num_samples // 2, 2) + torch.tensor([-2.0, -2.0])
    input_data = torch.cat([category_1, category_2], dim=0)
    # Create target data (categories)
    target_data = torch.cat([torch.zeros(num_samples // 2), torch.ones(num_samples // 2)]).long()
    return input_data, target_data

def generate_spiral_data(num_samples, num_turns=2, noise_std=0.75):
    # Generate a positive and a negative spiral
    t = torch.linspace(0, 1, num_samples)
    t = t * num_turns * 2 * math.pi  # Adjust the number of turns

    # Positive spiral
    x1 = t * torch.cos(t)
    y1 = t * torch.sin(t)

    # Negative spiral
    x2 = -t * torch.cos(t)
    y2 = -t * torch.sin(t)

    # Add noise to the data
    x1 += torch.randn_like(x1) * noise_std
    y1 += torch.randn_like(y1) * noise_std
    x2 += torch.randn_like(x2) * noise_std
    y2 += torch.randn_like(y2) * noise_std

    # Combine the positive and negative spirals
    input_data = torch.cat([torch.stack((x1, y1), dim=1), torch.stack((x2, y2), dim=1)], dim=0)

    # Create target data (0 for positive spiral, 1 for negative spiral)
    target_data = torch.cat([torch.zeros(num_samples), torch.ones(num_samples)]).long()

    return input_data, target_data

input_data, target_data = generate_spiral_data(200, noise_std=0.25)
print(target_data)

# Modify the network to accept 2D input data
modules_dict = {
    "encoder": {
        "module": modules.FullBlock([2, 32, 16, 8], activation='swish')  # 2 input features
    },
    "compute": {
        "module": modules.FullBlock([8, 8, 8], activation='swish')
    },
    "decoder": {
        "module": modules.FullBlock([8, 16, 32, 2], activation='swish')  # 2 output features
    }
}

network = modules.GeneralNetwork(modules_dict)

# Training loop with visualization
optimizer = torch.optim.Adam(network.parameters(), lr=0.001)
loss_function = nn.CrossEntropyLoss()  # Cross-entropy loss for classification

# Lists to store losses and predictions for visualization
losses = []
predictions = []

for epoch in range(2000):  # Training for 100 epochs
    optimizer.zero_grad()
    outputs = network(input_data)
    loss = loss_function(outputs, target_data)
    loss.backward()
    optimizer.step()

    if epoch % 10 == 0:
        print(f'Epoch {epoch}, Loss: {loss.item()}')
        losses.append(loss.item())
        # Calculate and store network predictions for visualization
        with torch.no_grad():
            predicted_category = torch.argmax(outputs, dim=1).numpy()
            predictions.append(predicted_category)

# Visualization of input data and network predictions
plt.figure(figsize=(18, 6))  # Adjust the figsize to accommodate three plots

# Plot input data points colored by true categories
plt.subplot(1, 3, 1)  # Three plots in a single row
plt.scatter(input_data[:, 0], input_data[:, 1], c=target_data.numpy(), cmap=plt.cm.Spectral)
plt.title("True Categories")

# Plot input data points colored by predicted categories
plt.subplot(1, 3, 2)
if len(predictions) > 0:
    plt.scatter(input_data[:, 0], input_data[:, 1], c=predictions[-1], cmap=plt.cm.Spectral)
    plt.title("Predicted Categories (Final Epoch)")
else:
    plt.title("Predicted Categories")

# Visualization of categorization of the entire 100x100 space
x_values = np.linspace(-5, 5, 100)
y_values = np.linspace(-5, 5, 100)
xx, yy = np.meshgrid(x_values, y_values)
grid_data = np.column_stack((xx.ravel(), yy.ravel()))
grid_tensor = torch.tensor(grid_data, dtype=torch.float32)
with torch.no_grad():
    grid_outputs = network(grid_tensor)
    grid_predictions = torch.argmax(grid_outputs, dim=1).numpy()
grid_predictions = grid_predictions.reshape(xx.shape)

plt.subplot(1, 3, 3)  # Third plot
plt.imshow(grid_predictions, extent=(-5, 5, -5, 5), origin='lower', cmap=plt.cm.Spectral, alpha=0.5)
plt.title("Categorization of Entire 100x100 Space")

plt.show()
