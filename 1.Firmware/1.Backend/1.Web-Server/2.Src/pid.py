import pygame
import random
import sys
import math
import time

# ======================================================================================== #

import random
from itertools import count

import torch
import torch.nn as nn
import torch.optim as optim

from memory import ReplayMemory, Transition
from linear import DQN

from collections import namedtuple, deque

# Define the Replay Memory and the DQN model (neural network)
Transition = namedtuple('Transition', ('state', 'action', 'next_state', 'reward'))

class ReplayMemory(object):
    def __init__(self, capacity):
        self.memory = deque([], maxlen=capacity)

    def push(self, *args):
        self.memory.append(Transition(*args))

    def sample(self, batch_size):
        return random.sample(self.memory, batch_size)

    def __len__(self):
        return len(self.memory)

class DQN(nn.Module):
    def __init__(self, input_size, output_size):
        super(DQN, self).__init__()
        # Define the neural network structure here
        # Example: Two hidden layers with ReLU activation
        self.fc1 = nn.Linear(input_size, 128)
        self.fc2 = nn.Linear(128, 64)
        self.fc3 = nn.Linear(64, output_size)

    def forward(self, x):
        x = torch.relu(self.fc1(x))
        x = torch.relu(self.fc2(x))
        return self.fc3(x)
    

# ======================================================================================== #

# Initialize Pygame
pygame.init()

# Grid and chart configuration
GRID_ROWS, GRID_COLS = 3, 2  # Grid layout
SCREEN_WIDTH, SCREEN_HEIGHT = 1600, 800
CHART_WIDTH, CHART_HEIGHT = SCREEN_WIDTH // GRID_COLS, SCREEN_HEIGHT // GRID_ROWS

# Color constants
WHITE = (255, 255, 255)
BLUE = (0, 0, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)

# Chart settings for each instance
CHART_SETTINGS = [
    {'internal_width': 100, 'point_gap': 5, 'move_speed': .2, 'randomness_range': (-10, 10), 'closest_points': 10, 'y_value_range': (0, CHART_HEIGHT), 'thrust_force': 1000},
    {'internal_width': 1000, 'point_gap': 50, 'move_speed': 2, 'randomness_range': (-10, 10), 'closest_points': 10, 'y_value_range': (0, CHART_HEIGHT), 'thrust_force': 1000},
    {'internal_width': 1000, 'point_gap': 50, 'move_speed': 2, 'randomness_range': (-10, 10), 'closest_points': 10, 'y_value_range': (0, CHART_HEIGHT), 'thrust_force': 1000},
    {'internal_width': 1000, 'point_gap': 50, 'move_speed': 2, 'randomness_range': (-10, 10), 'closest_points': 10, 'y_value_range': (0, CHART_HEIGHT), 'thrust_force': 1000},
    {'internal_width': 1000, 'point_gap': 50, 'move_speed': 2, 'randomness_range': (-10, 10), 'closest_points': 10, 'y_value_range': (0, CHART_HEIGHT), 'thrust_force': 1000},
    {'internal_width': 1000, 'point_gap': 50, 'move_speed': 2, 'randomness_range': (-10, 10), 'closest_points': 10, 'y_value_range': (0, CHART_HEIGHT), 'thrust_force': 1000},
    # Add more configurations here...
]


class DynamicChart:
    def __init__(self, settings):
        self.settings = settings
        self.points = [(self.settings['internal_width'], CHART_HEIGHT // 2)]
        self.font = pygame.font.SysFont(None, 24)
        self.history = []
        self.velocity = 0

    def generate_point(self, last_point):
        variation = random.uniform(*self.settings['randomness_range'])
        new_y = last_point[1] + variation
        return (last_point[0] + self.settings['point_gap'], new_y)
    
    def apply_thrust(self, direction, delta_time):
        self.velocity += self.settings['thrust_force'] * direction * delta_time

    def update_points(self, delta_time):
        self.points = [(x - self.settings['move_speed'], y - self.velocity * delta_time) for x, y in self.points]
        if self.points[-1][0] < self.settings['internal_width'] - self.settings['point_gap']:
            self.points.append(self.generate_point(self.points[-1]))
        while self.points and self.points[0][0] < 0:
            self.points.pop(0)

    def find_closest_points(self, agent, number):
        points_distances = [(point, math.sqrt((point[0] - agent[0])**2 + (point[1] - agent[1])**2)) for point in self.points]
        points_distances.sort(key=lambda x: x[1])
        return [point for point, _ in points_distances[:number]]

    def draw(self, screen, offset_x, offset_y):
        scale_x = CHART_WIDTH / self.settings['internal_width']
        agent = (self.settings['internal_width'] // 2, CHART_HEIGHT // 2)
        pygame.draw.circle(screen, RED, (agent[0]*scale_x + offset_x, agent[1]+offset_y), 3)

        for i in range(1, len(self.points)):
            point1 = (self.points[i - 1][0] * scale_x + offset_x, self.points[i - 1][1] + offset_y)
            point2 = (self.points[i][0] * scale_x + offset_x, self.points[i][1] + offset_y)

            # Only draw if points are within the chart height
            if offset_y <= point1[1] <= CHART_HEIGHT + offset_y and offset_y <= point2[1] <= CHART_HEIGHT + offset_y:
                pygame.draw.line(screen, BLUE, point1, point2, 2)
                pygame.draw.circle(screen, BLUE, point2, 3)

        closest_points = self.find_closest_points(agent, self.settings['closest_points'])
        for point in closest_points:
            adjusted_point = (point[0] * scale_x + offset_x, point[1] + offset_y)
            if offset_y <= adjusted_point[1] <= CHART_HEIGHT + offset_y:
                pygame.draw.line(screen, RED, (agent[0] * scale_x + offset_x, agent[1] + offset_y), adjusted_point, 1)

        latest_y_value = f"Y: {self.points[-1][1]-agent[1]:.2f}"
        text_surface = self.font.render(latest_y_value, True, BLACK)
        text_rect = text_surface.get_rect(center=(100 + offset_x, 50 + offset_y))
        pygame.draw.rect(screen, WHITE, text_rect.inflate(20, 10))
        screen.blit(text_surface, text_rect)

    def get_state(self):
        # Returns the current state of the chart, which can be used as input for AI
        # For simplicity, let's just use the y-coordinate of the last point
        agent = (self.settings['internal_width'] // 2, CHART_HEIGHT // 2)
        closest_points = self.find_closest_points(agent, self.settings['closest_points'])
        rel_closest_points = [(point[0]-agent[0], point[1]-agent[1]) for point in closest_points]
        return {'points': rel_closest_points, 'velocity': self.velocity}

    def apply_action(self, action, delta_time):
        # Maps an action from the AI to the chart controls (e.g., thrust)
        # Action could be a discrete or continuous value depending on the AI model
        self.apply_thrust(action, delta_time)

    def get_extended_state(self):
        # This method returns the current state along with a history of past states
        # Assuming a fixed memory size for simplicity
        memory_size = 5  # number of past states to remember
        if len(self.history) < memory_size:
            return {'points': self.get_state()['points'], 'velocity': self.velocity, 'history': list(self.history)}
        else:
            return {'points': self.get_state()['points'], 'velocity': self.velocity, 'history': list(self.history)[-memory_size:]}

    def update_history(self):
        self.history.append(self.get_state())


def get_chart_at_position(charts, position):
    for i, chart in enumerate(charts):
        chart_x = (i % GRID_COLS) * CHART_WIDTH
        chart_y = (i // GRID_COLS) * CHART_HEIGHT
        if chart_x <= position[0] < chart_x + CHART_WIDTH and chart_y <= position[1] < chart_y + CHART_HEIGHT:
            return i
    return None

# Initialize charts with only settings, no position
charts = [DynamicChart(settings) for settings in CHART_SETTINGS]
selected_chart_index = None

# Pygame screen setup
screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
pygame.display.set_caption("Multiple Dynamic Charts")
clock = pygame.time.Clock()

def main():
    global selected_chart_index
    last_time = time.time()

    while True:
        current_time = time.time()
        delta_time = current_time - last_time
        last_time = current_time

        keys = pygame.key.get_pressed()
        if selected_chart_index is not None:
            if keys[pygame.K_UP]:
                charts[selected_chart_index].apply_thrust(-1, delta_time)
            elif keys[pygame.K_DOWN]:
                charts[selected_chart_index].apply_thrust(1, delta_time)
    
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            if event.type == pygame.MOUSEBUTTONDOWN:
                clicked_chart_index = get_chart_at_position(charts, pygame.mouse.get_pos())
                if clicked_chart_index is not None:
                    selected_chart_index = clicked_chart_index

        screen.fill(WHITE)
        for i, chart in enumerate(charts):
            offset_x, offset_y = ((i % GRID_COLS) * CHART_WIDTH, (i // GRID_COLS) * CHART_HEIGHT)
            chart.update_points(delta_time)
            chart.draw(screen, offset_x, offset_y)
            if i == selected_chart_index:
                pygame.draw.rect(screen, RED, (offset_x, offset_y, CHART_WIDTH, CHART_HEIGHT), 2)

        pygame.display.flip()
        clock.tick(60)

# ======================================================================================== #

BATCH_SIZE = 128
GAMMA = 0.99
EPS_START = 0.9
EPS_END = 0.05
EPS_DECAY = 1000
TAU = 0.005
LR = 1e-4 # learning rate

# use gpu if available
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

# Define the number of actions and observations
n_actions = 2  # Example: 0 for no thrust, 1 for thrust
n_observations = len(CHART_SETTINGS[0]['closest_points']) * 2 + 1 + 5 * (len(CHART_SETTINGS[0]['closest_points']) * 2 + 1)  # current state + 5 history states

policy_net = DQN(n_observations, n_actions).to(device)
target_net = DQN(n_observations, n_actions).to(device)
target_net.load_state_dict(policy_net.state_dict())
optimizer = optim.Adam(policy_net.parameters(), lr=LR)
memory = ReplayMemory(10000)

steps_done = 0

def select_action(state):
    global steps_done
    sample = random.random()
    eps_threshold = EPS_END + (EPS_START - EPS_END) * math.exp(-1. * steps_done / EPS_DECAY)
    steps_done += 1
    if sample > eps_threshold:
        with torch.no_grad():
            return policy_net(state).max(1)[1].view(1, 1)
    else:
        # return random action
        # TODO
        pass

def calculate_reward(chart):
    # Constants for reward calculation
    MAX_DISTANCE = CHART_HEIGHT  # Maximum possible distance in the chart
    DISTANCE_WEIGHT = 0.4  # Weight for the distance component of the reward
    VELOCITY_WEIGHT = 0.3  # Weight for the velocity component of the reward
    STABILITY_WEIGHT = 0.3  # Weight for the stability component of the reward

    # Calculate distance reward
    state_info = chart.get_extended_state()
    distances = [math.sqrt(x**2 + y**2) for x, y in state_info['points']]
    avg_distance = sum(distances) / len(distances)
    distance_reward = (MAX_DISTANCE - avg_distance) / MAX_DISTANCE

    # Calculate velocity reward
    velocity = state_info['velocity']
    velocity_reward = 1 - min(abs(velocity) / MAX_DISTANCE, 1)
    if avg_distance > MAX_DISTANCE / 2:
        velocity_reward *= -1  # Encourage high velocity when far away

    # Calculate stability reward
    # Assuming we have a way to calculate the rate of change of distance
    rate_of_change = calculate_rate_of_change(state_info)  # Define this function
    stability_reward = 1 - min(rate_of_change / MAX_DISTANCE, 1)
    if avg_distance < MAX_DISTANCE / 4:
        stability_reward *= -1  # Discourage high rate of change when close

    # Combined weighted reward
    total_reward = (DISTANCE_WEIGHT * distance_reward +
                    VELOCITY_WEIGHT * velocity_reward +
                    STABILITY_WEIGHT * stability_reward)
    return total_reward

def calculate_rate_of_change(current_state):
    history = current_state['history']

    # Retrieve the last state from history
    last_state = history[-1]

    # Calculate average distance for the current state
    current_distances = [math.sqrt(x**2 + y**2) for x, y in current_state['points']]
    current_avg_distance = sum(current_distances) / len(current_distances)

    # Calculate average distance for the last state
    last_distances = [math.sqrt(x**2 + y**2) for x, y in last_state['points']]
    last_avg_distance = sum(last_distances) / len(last_distances)

    # Rate of change of distance
    rate_of_change = current_avg_distance - last_avg_distance
    return rate_of_change

def optimize_model():
    if len(memory) < BATCH_SIZE:
        return
    transitions = memory.sample(BATCH_SIZE)
    batch = Transition(*zip(*transitions))

    non_final_mask = torch.tensor(tuple(map(lambda s: s is not None, batch.next_state)), device=device, dtype=torch.bool)
    non_final_next_states = torch.cat([s for s in batch.next_state if s is not None])

    state_batch = torch.cat(batch.state)
    action_batch = torch.cat(batch.action)
    reward_batch = torch.cat(batch.reward)

    state_action_values = policy_net(state_batch).gather(1, action_batch)

    next_state_values = torch.zeros(BATCH_SIZE, device=device)
    with torch.no_grad():
        next_state_values[non_final_mask] = target_net(non_final_next_states).max(1)[0]

    expected_state_action_values = (next_state_values * GAMMA) + reward_batch

    criterion = nn.SmoothL1Loss()
    loss = criterion(state_action_values, expected_state_action_values.unsqueeze(1))

    optimizer.zero_grad()
    loss.backward()
    torch.nn.utils.clip_grad_value_(policy_net.parameters(), 100)
    optimizer.step()

if torch.cuda.is_available():
    num_episodes = 600
else:
    num_episodes = 50

def train():
    global selected_chart_index
    last_time = time.time()

    while True:
        current_time = time.time()
        delta_time = current_time - last_time
        last_time = current_time

        # AI Control Logic
        for chart in charts:
            extended_state = chart.get_extended_state()
            current_state = extended_state['points']
            history = extended_state['history']
            rate_of_change = calculate_rate_of_change(current_state, history)

            state_tensor = torch.tensor([current_state], device=device, dtype=torch.float32)
            action = select_action(state_tensor)
            chart.apply_action(action.item(), delta_time)

            reward = calculate_reward(chart, rate_of_change)
            # Store state, action, reward, and next state in memory, then optimize model
            # ...

# Run the application
if __name__ == '__main__':
    main()