import torch
import torch.nn as nn
import torch.nn.functional as F
import math

# ================================================================================================================================================= #

class CustomSensitivityLayer(nn.Module):
    def __init__(self, in_features, out_features):
        super(CustomSensitivityLayer, self).__init__()
        self.weight = nn.Parameter(torch.Tensor(out_features, in_features))
        self.range_sensitivity = nn.Parameter(torch.Tensor(out_features, in_features))
        nn.init.kaiming_uniform_(self.weight, a=math.sqrt(5))
        nn.init.constant_(self.range_sensitivity, 1)

    def forward(self, input):
        adjusted_weight = self.weight * self.range_sensitivity
        return F.linear(input, adjusted_weight)

class Swish(nn.Module):
    def __init__(self, learnable=True):
        super(Swish, self).__init__()
        self.beta = nn.Parameter(torch.tensor(1.0)) if learnable else torch.tensor(1.0)

    def forward(self, x):
        return x * torch.sigmoid(self.beta * x)

def create_layers(layer_sizes, activation='leaky_relu'):
    layers = []
    for i in range(len(layer_sizes) - 1):
        layers.append(CustomSensitivityLayer(layer_sizes[i], layer_sizes[i + 1]))
        if activation == 'leaky_relu':
            layers.append(nn.LeakyReLU(0.01))
        elif activation == 'swish':
            layers.append(Swish(learnable=True))
    return nn.Sequential(*layers)

# ================================================================================================================================================= #

class FullBlock(nn.Module):
    def __init__(self, layer_sizes, activation='leaky_relu'):
        super(FullBlock, self).__init__()
        self.layers = create_layers(layer_sizes, activation)

    def forward(self, x):
        return self.layers(x)

# ================================================================================================================================================= #

class GeneralNetwork(nn.Module):
    def __init__(self, modules_dict):
        super(GeneralNetwork, self).__init__()
        self.modules_dict = modules_dict
        self.modules_list = nn.ModuleList()
        self.reshaping_funcs = {}

        for module_name, module_info in modules_dict.items():
            self.modules_list.append(module_info['module'])
            # Store the reshaping function if it exists
            if 'reshape_func' in module_info:
                self.reshaping_funcs[module_name] = module_info['reshape_func']

    def forward(self, x):
        for i, module in enumerate(self.modules_list):
            module_name = list(self.modules_dict.keys())[i]
            # Reshape x if a reshaping function is provided for this module
            if module_name in self.reshaping_funcs:
                x = self.reshaping_funcs[module_name](x)
            x = module(x)
        return x
