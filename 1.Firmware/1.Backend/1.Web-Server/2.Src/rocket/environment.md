# LineFollowerEnv

`LineFollowerEnv` is a custom environment for OpenAI's Gym that simulates a line-following robot. The environment is designed to train reinforcement learning agents to follow a randomly generated path by observing checkpoints and making appropriate turns to stay as close to the path as possible.

## Environment Details

- **Action Space**: The agent can perform two discrete actions:
  - `0`: Turn left by a fixed angle.
  - `1`: Turn right by a fixed angle.

- **Observation Space**: The observation is a 37-dimensional vector containing:
  - The relative positions of the next three checkpoints.
  - The directions of the line segments between the checkpoints and an additional weighted average direction for the central checkpoint.
  - The orientation of the agent.
  - A history of the last five full observations (without history).

- **Parameters**:
  - `num_points`: Number of checkpoints to generate on the track.
  - `robot_speed`: Speed of the robot.
  - `turn_angle`: Angle by which the robot turns for each action.
  - `threshold_distance`: Distance threshold to determine if a checkpoint has been passed.
  - `reward_constant`: Scaling factor for the reward.

## Reward Function

The reward function is designed to encourage the agent to align its movement direction with the direction to the next checkpoint. It comprises several components:

1. **Direction Reward**: Inverse of the absolute norm of the difference between the target direction at the first checkpoint and the current direction of the agent, scaled by the distance to the first checkpoint and a scalar constant.

2. **Distance Reward**: Negative of the relative distance to the first checkpoint to encourage the agent to get closer.

3. **Direction Change Reward**: Additional reward for increasing alignment with the target direction.

4. **Distance Change Reward**: Additional reward for decreasing distance to the first checkpoint.

5. **Confidence/Stability Policy**: Penalty for finding solutions that deviate from the expected path, preventing exploitation of unintended strategies.

## Functions

- `reset()`: Resets the environment to an initial state.
- `step(action)`: Advances the environment by one timestep using the given action.
- `render(mode='human')`: Renders the current state of the environment. Useful for visualizing the agent's progress.
- `close()`: Closes any external windows or processes started by the environment.

## Usage

To use the `LineFollowerEnv`, instantiate it and interact with it using the standard Gym interface.

```python
import gym
from line_follower_env import LineFollowerEnv

# Create the environment
env = LineFollowerEnv()

# Reset the environment
observation = env.reset()

# Sample a random action
action = env.action_space.sample()

# Take a step in the environment
observation, reward, done, info = env.step(action)

# Render the environment
env.render()

# Close the environment
env.close()
