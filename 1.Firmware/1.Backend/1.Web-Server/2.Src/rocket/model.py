import torch
import stable_baselines3 as sb3
from stable_baselines3.common.torch_layers import BaseFeaturesExtractor as sb3_BaseFeaturesExtractor
from stable_baselines3.common.policies import ActorCriticPolicy as sb3_ActorCriticPolicy
import modules as lib_modules
import os


class CustomFeatureExtractor(sb3_BaseFeaturesExtractor):
    def __init__(self, observation_space, modules_dict, features_dim):
        super(CustomFeatureExtractor, self).__init__(observation_space, features_dim=features_dim)  # Set features_dim to your output size

        self.network = lib_modules.GeneralNetwork(modules_dict)
        self.modules_dict = modules_dict

    def forward(self, observations):
        return self.network(observations)

class CustomActorCriticPolicy(sb3_ActorCriticPolicy):
    def __init__(self, observation_space, action_space, lr_schedule, features_extractor_class=CustomFeatureExtractor, modules_dict=None, features_dim=None, **kwargs):
        super(CustomActorCriticPolicy, self).__init__(
            observation_space,
            action_space,
            lr_schedule,
            features_extractor_class=features_extractor_class,
            features_extractor_kwargs={"modules_dict": modules_dict, "features_dim": features_dim},
            **kwargs
        )


def stages(stages_config):
    """
    Generates stage shapes and arguments for each stage based on the provided configurations.
    
    :param stages_config: List of dictionaries, each specifying a stage configuration.
                          Example stage config: {'type': 'funnel', 'start_size': 64, 'end_size': 16, 'multiplier': 0.9, 'activation': 'swish', 'layer': 'custom'}
                          Or for a block stage: {'type': 'block', 'size': 32, 'depth': 4, 'activation': 'relu', 'layer': 'custom'}
    :return: List of dictionaries with layer sizes and arguments for each stage.
    """
    stages_output = []
    for stage in stages_config:
        stage_info = {}
        if stage['type'] == 'funnel':
            sizes = [stage['start_size']]
            is_downscaling = stage['start_size'] > stage['end_size']

            while ((is_downscaling and sizes[-1] > stage['end_size']) or 
                   (not is_downscaling and sizes[-1] < stage['end_size'])):
                if is_downscaling:
                    next_size = max(sizes[-1] * stage['multiplier'], stage['end_size'])
                else:
                    next_size = min(sizes[-1] / stage['multiplier'], stage['end_size'])
                if int(next_size) != int(sizes[-1]):
                    sizes.append(next_size)
                else:
                    sizes[-1] = next_size

        elif stage['type'] == 'block':
            sizes = [stage['size']] * stage['depth']
        
        stage_info['activation'] = stage.get('activation', 'swish')
        stage_info['layer'] = stage.get('layer', 'custom')
        stage_info['shape'] = [int(size) for size in sizes]
        stages_output.append(stage_info)

    return stages_output

def modules(stages):
    """
    Generates a modules_dict using the output from the stages function.

    :param stages: Output from the stages function.
    :return: modules_dict with constructed modules for each stage.
    """
    modules_dict = {}
    for i, stage in enumerate(stages):
        stage_name = f"stage_{i}"
        activation = stage.get('activation', 'swish')
        layer_type = stage.get('layer', 'default')

        module = lib_modules.FullBlock(stage['shape'], activation=activation, layer_type=layer_type)
        modules_dict[stage_name] = {"module": module}
    
    return modules_dict


def save(model, save_path):
    os.makedirs(os.path.dirname(save_path), exist_ok=True)
    torch.save(model.policy.state_dict(), save_path + "_policy_parameters.pth")
    custom_config = {
        "modules_dict": model.policy.features_extractor.modules_dict,
        "features_dim": model.policy.features_extractor.features_dim
    }
    torch.save(custom_config, save_path + "_config.pth")

def load(load_path, env):
    custom_config = torch.load(load_path + "_config.pth")
    modules_dict = custom_config["modules_dict"]
    features_dim = custom_config["features_dim"]

    model = sb3.PPO(CustomActorCriticPolicy, env, policy_kwargs={"modules_dict": modules_dict, "features_dim": features_dim})
    model.policy.load_state_dict(torch.load(load_path + "_policy_parameters.pth"))

    return model

def create(modules_dict, features_dim, env):
    model = sb3.PPO(CustomActorCriticPolicy, env, policy_kwargs={"modules_dict": modules_dict, "features_dim": features_dim})
    return model
