import time
import os
import model as mdl
import environment
import stable_baselines3 as sb3

models_dir = f"models/ppo-{int(time.time())}"
log_dir = f"logs/ppo-{int(time.time())}"

if not os.path.exists(models_dir):
    os.makedirs(models_dir)

if not os.path.exists(log_dir):
    os.makedirs(log_dir)

env = environment.LineFollowerEnv(render_step=True)
env.reset()


num_observations = env.observation_space.shape[0]
num_actions = env.action_space.n

print("[DEBUG] Num of observations:", num_observations)
print("[DEBUG] Num of actions:", num_actions)

stages_config = [
    {'type': 'funnel', 'start_size': num_observations, 'end_size': num_observations*10, 'multiplier': 0.5, 'activation': 'leaky_relu', 'layer': 'normal'},
    {'type': 'block', 'size': num_observations*10, 'depth': 2, 'activation': 'leaky_relu', 'layer': 'normal'},
    {'type': 'funnel', 'start_size': num_observations*10, 'end_size': num_actions, 'multiplier': 0.5, 'activation': 'leaky_relu', 'layer': 'normal'},
]

print("[DEBUG] Stages config:", stages_config)

stages = mdl.stages(stages_config)
modules = mdl.modules(stages)

print("[DEBUG] Stages:", stages)
print("[DEBUG] Modules:", modules)

model = sb3.PPO(mdl.CustomActorCriticPolicy, env, verbose=1, tensorboard_log=log_dir, policy_kwargs={"modules_dict": modules, "features_dim": num_actions}, ent_coef=0.001)
# model = sb3.PPO("MlpPolicy", env, verbose=1, tensorboard_log=log_dir, ent_coef=0.001)

TIMESTEPS = 100000
for i in range(10000):
    print("[DEBUG] Episode:", i)
    model.learn(total_timesteps=TIMESTEPS, reset_num_timesteps=False, tb_log_name="ppo", progress_bar=True)
    env.reset()
    # mdl.save(model, f"{models_dir}/{i}")
env.close()