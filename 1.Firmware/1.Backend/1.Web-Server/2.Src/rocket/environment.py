import gym
import numpy as np
import cv2
import math
from collections import deque

class LineFollowerEnv(gym.Env):
    metadata = {'render.modes': ['human']}

    def __init__(self, render_step=False, num_points=50, robot_speed=1, turn_angle=10, threshold_distance=50, reward_constant=1):
        super(LineFollowerEnv, self).__init__()

        self.action_space = gym.spaces.Discrete(3)  # Actions: Turn left, Turn right
        # Observation space: 6 for checkpoints (x, y for each of the 3 checkpoints), 1 for orientation, 24 for PID history
        # self.observation_space = gym.spaces.Box(low=-1000, high=1000, shape=(34,), dtype=np.float32)
        self.observation_space = gym.spaces.Box(low=-1000, high=1000, shape=(3,), dtype=np.float32)

        # Customizable parameters
        self.num_points = num_points
        self.robot_speed = robot_speed
        self.turn_angle = turn_angle
        self.threshold_distance = threshold_distance
        self.reward_constant = reward_constant

        self.render_step = render_step
        self.robot_position = np.array([50, 50], dtype=np.float32)
        self.robot_orientation = 0

        self.global_checkpoints = self._generate_track()
        self.relative_checkpoints = self._calculate_relative_checkpoints()

        # Fitness variables
        self.previous_distance = float('inf')
        self.previous_direction_difference = float('inf')

        # PID controller variables
        self.observation_size = 7

    def _generate_checkpoints(self):
        return np.random.randint(0, 1000, size=(self.num_points, 2))

    def _generate_track(self):
        checkpoints = self._generate_checkpoints()
        track = [self.robot_position]

        while len(checkpoints) > 0:
            current_point = track[-1]
            distances = np.linalg.norm(checkpoints - current_point, axis=1)
            closest_idx = np.argmin(distances)
            track.append(checkpoints[closest_idx])
            checkpoints = np.delete(checkpoints, closest_idx, 0)

        return np.array(track[1:], dtype=np.float32)  # Exclude the robot's position

    def _calculate_relative_checkpoints(self):
        relative_positions = self.robot_position - self.global_checkpoints
        return relative_positions

    def _get_next_directions(self):
        # Calculate the directions of the line segments between the checkpoints
        next_directions = [
            np.arctan2(
                self.relative_checkpoints[0][1] - self.relative_checkpoints[1][1],
                self.relative_checkpoints[0][0] - self.relative_checkpoints[1][0]
            ),
            None,
            np.arctan2(
                self.relative_checkpoints[1][1] - self.relative_checkpoints[2][1],
                self.relative_checkpoints[1][0] - self.relative_checkpoints[2][0]
            ),

        ]
        next_directions[1] = (next_directions[0] + next_directions[2])/2 % 2*np.pi

        return np.array(next_directions)

    def _get_observation(self):
        directions = self._get_next_directions().flatten()

        observation = np.array([self.relative_checkpoints[0][0], self.relative_checkpoints[0][1] , (self.robot_orientation - directions[0]) % (2*np.pi)])
        self.observation = observation
        return observation

    def _update_checkpoints(self):
        # TODO: better alternative and previous direction?
        if self._has_passed_checkpoint():
            self.global_checkpoints = np.delete(self.global_checkpoints, 0, 0)
            self.previous_distance = np.linalg.norm(self.relative_checkpoints[0])

    def _has_passed_checkpoint(self):
        # TODO: better alternative if there is a following checpoint than the first one and not part of the already selected 3
        if len(self.global_checkpoints) == 0:
            return False

        distance_to_checkpoint = np.linalg.norm(self.relative_checkpoints[0])

        if distance_to_checkpoint < self.previous_distance and distance_to_checkpoint < self.threshold_distance:
            return True

        return False

    def _get_reward(self):
        # Weightings
        current_direction_weight = 0.2
        direction_reward_weight = 0.2
        distance_reward_weight = 0.2
        change_in_direction_reward_weight = 0.2
        change_in_distance_reward_weight = 0.2
        smoothness_weight = 0.1  # New weight for trajectory smoothness

        # Calculate the target and current directions
        target_direction = np.arctan2(self.relative_checkpoints[0][1], self.relative_checkpoints[0][0])
        current_direction = np.radians(self.robot_orientation)

        # Direction Reward
        direction_difference = np.linalg.norm(target_direction - current_direction)
        direction_reward = (np.cos(direction_difference) + 1) / 2  # Normalized between [0, 1]

        # Distance Reward
        distance_to_checkpoint = np.linalg.norm(self.relative_checkpoints[0])
        distance_reward = 1 / (1 + distance_to_checkpoint)  # Approaches 1 as distance decreases

        # Change in Direction and Distance Reward
        change_in_direction_reward = (self.previous_direction_difference - direction_difference)
        change_in_direction_reward = max(min(change_in_direction_reward, 1), 0)  # Normalize

        change_in_distance_reward = (self.previous_distance - distance_to_checkpoint)
        change_in_distance_reward = max(min(change_in_distance_reward, 1), 0)  # Normalize

        # # Trajectory Smoothness (Second and Third Derivatives)
        # acceleration = np.diff(self.history[:, -3])  # Assuming -3 is the velocity index
        # jerk = np.diff(acceleration)
        # smoothness_penalty = np.std(jerk)  # Higher std indicates less smooth trajectory

        # Update history
        self.previous_direction_difference = direction_difference
        self.previous_distance = distance_to_checkpoint

        # Total Reward Calculation
        total_reward = (direction_reward_weight * direction_reward +
                        distance_reward_weight * distance_reward +
                        change_in_direction_reward_weight * change_in_direction_reward +
                        change_in_distance_reward_weight * change_in_distance_reward -
                        smoothness_weight) #* smoothness_penalty)

        self.reward = total_reward
        return total_reward

    def _is_done(self):
        return (self.robot_position[0] < 0 or self.robot_position[0] > 1000 or self.robot_position[1] < 0 or self.robot_position[1] > 1000)


    def reset(self):
        self.robot_orientation = 0
        self.robot_position = np.array([500, 500], dtype=np.float32)
        self.global_checkpoints = self._generate_track()
        self.relative_checkpoints = self._calculate_relative_checkpoints()
        self.previous_distance = np.linalg.norm(self.relative_checkpoints[0])
        self.previous_direction_difference = 0
        observation = self._get_observation()
        return observation
    
    def _take_action(self, action):
        # TODO: Quantitative
        self.action = action
        if action == 0:  # Turn left
            self.robot_orientation -= np.radians(self.turn_angle)
        elif action == 1:  # Turn right
            self.robot_orientation += np.radians(self.turn_angle)

        # Move forward
        self.robot_orientation = self.robot_orientation % 2*np.pi
        angle_rad = self.robot_orientation
        self.robot_position += np.array([np.cos(angle_rad), np.sin(angle_rad)], dtype=np.float32) * self.robot_speed

    def step(self, action):
        self._take_action(action)
        self.relative_checkpoints = self._calculate_relative_checkpoints()
        self._update_checkpoints()
        
        reward = self._get_reward()
        observation = self._get_observation()
        done = self._is_done()

        if self.render_step:
            self.render()

        return observation, reward, done, {}

    def render(self, mode='human'):
        img = np.zeros((1000, 1000, 3), dtype=np.uint8)

        # Draw the checkpoints
        for checkpoint in self.global_checkpoints:
            cv2.circle(img, tuple(checkpoint.astype(int)), 3, (0, 255, 0), -1)

        # Draw the trajectory based on the next three checkpoints
        if len(self.global_checkpoints) >= 3:
            pts = self.global_checkpoints[:3].astype(int)
            cv2.polylines(img, [pts], False, (0, 0, 255), 1)

        # Draw the robot
        cv2.circle(img, tuple(self.robot_position.astype(int)), 5, (0, 0, 255), -1)
        cv2.putText(img, f"Orientation: {self.robot_orientation}", (10,30), cv2.FONT_HERSHEY_SIMPLEX, 1, (255,255,255), 2, cv2.LINE_AA)
        cv2.putText(img, f"Orientation Checkpoint: {self._get_next_directions()[0]}", (10,60), cv2.FONT_HERSHEY_SIMPLEX, 1, (255,255,255), 2, cv2.LINE_AA)
        cv2.putText(img, f"Relative Checkpoints: {self.relative_checkpoints[:3]}", (10,90), cv2.FONT_HERSHEY_SIMPLEX, 1, (255,255,255), 2, cv2.LINE_AA)
        cv2.putText(img, f"Reward: {self.reward}", (10,120), cv2.FONT_HERSHEY_SIMPLEX, 1, (255,255,255), 2, cv2.LINE_AA)
        cv2.putText(img, f"Action: {self.action}", (10,150), cv2.FONT_HERSHEY_SIMPLEX, 1, (255,255,255), 2, cv2.LINE_AA)
        cv2.putText(img, f"Obs: {self.observation}", (10,180), cv2.FONT_HERSHEY_SIMPLEX, 1, (255,255,255), 2, cv2.LINE_AA)
        cv2.imshow("Line Follower", img)
        cv2.waitKey(1)

    def close(self):
        cv2.destroyAllWindows()