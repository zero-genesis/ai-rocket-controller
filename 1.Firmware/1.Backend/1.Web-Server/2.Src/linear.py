import torch.nn as nn
import torch.nn.functional as F


class DQN(nn.Module):

    def __init__(self, n_observations, n_actions):
        super(DQN, self).__init__()
        self.layer1 = nn.Linear(n_observations, n_actions)
        self.layers = [self.layer1]

    def forward(self, x):
        for layer in self.layers:
            x = F.relu(self.layer(x))

        return x