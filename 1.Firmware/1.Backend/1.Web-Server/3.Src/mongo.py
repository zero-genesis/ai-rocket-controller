from pymongo import MongoClient

DATABASE = "AI-ROCKET-CONTROLLER"
COLLECTION_1 = "AI_STATE"
COLLECTION_2 = "ROCKET_STATE"


client = MongoClient("localhost", 27017)
db = client[DATABASE]

def save_ai_state(state):
    id = db[COLLECTION_1].insert_one(state).inserted_id
    return id

def save_rocket_state(state):
    id = db[COLLECTION_2].insert_one(state).inserted_id
    return id