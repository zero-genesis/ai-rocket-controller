# AI Rocket Controller

## Alternative training

### Self Reflection

Utilize two models, an interaction and observation model. The interaction model is responsible for interacting with the environment and produces actions based on observations. The experience model reflects on the previous observations, resulting actions and rewards and is responsible for coming up with and optimized reward policy.

A simpler alternative could be using a static/predetermined reward policy combined with a single or collection of previous model states and by comparing the actions and corresponding rewards of the current model with the previous models we get an overview of the wether the quality of the model has increased and based we can decide wether we scale the reward or not, additionally we can also go back to an older model or even make hybrid models which in turn could prevent false minima.